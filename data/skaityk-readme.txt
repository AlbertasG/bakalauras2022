PRIEDAS prie "Da�ninio lietuvi� ra�ytin�s kalbos �odyno"

Apie "dazninis.txt"

   Faile yra duomenys, kuri� pagrindu buvo sudarytas elektroninis "Da�ninis lietuvi� ra�ytin�s kalbos �odynas: 1 milijono �od�i� morfologi�kai anotuoto tekstyno pagrindu." Daugiau informacijos apie morfologin� anotuot� tekstyn� rasite elektroniniame leidinyje "Dazninis zodynas.pdf".

   Duomen� faile yra keturi stulpeliai:

   Antra�tinis �odis<TAB>Kalbos dalis<TAB>�od�io forma<TAB>Pavartojimo da�nis

   Duomenys sur��iuoti pagal antra�tinius �od�ius ab�c�l�s tvarka. Tokios strukt�ros duomen� failas lengvai atsidaro su skai�iuokli�-lenteli� programomis (pvz. MS Excel). I� viso �iame faile yra  128 058 �od�io formos.

   Teksto kuoduot� yra Baltic (Windows-1257)

   Elektroniniame �odyne antra�tini� �od�i� skirtingos formos yra sujungtos � vien� atitinkamo antra�tinio �od�io straipsn�, pateikiant bendr� antra�tinio �od�io da�n� ir atskir� form� da�nius.

Pvz. �emiau esantys duomenys:

aistra	dkt	aistra	15
aistra	dkt	aistrai	3
aistra	dkt	aistras	6
aistra	dkt	aistr�	1
aistra	dkt	aistroms	2
aistra	dkt	aistros	19
aistra	dkt	aistr�	8

Elektroniniame �odyne yra pateikiami taip:

2452. aistra  (dkt.)		54
aistros 19, aistra 15, aistr� 8, aistras 6, aistrai 3, aistroms 2, aistr� 1

--------------------------------------------------------

ANNEX to "Frequency Dictionary of Written Lithuanian - based on 1m word morphologically annotated corpus"

About "dazninis.txt"

   The file contains data, which is the basis for the electronic Lithuanian frequency dictionary

   There are four columns in the data file:

   Headword<TAB>Part of Speech<TAB>Wordform<TAB>Frequency of Occurrence

   The data is sorted by a headword according to the alphabetic order. So structured file can be easily opened by any spreadsheet program (e.g. MS Excel). In total the file contains 128,058 word forms.
  
   Text encoding is "Baltic (Windows-1257)".

   In the electronic dictionary different wordforms of one lemma are joined into one dictionary article under a headword with the frequencies of a headword and different wordforms.

   For example, the following lines of data:

aistra	dkt	aistra	15
aistra	dkt	aistrai	3
aistra	dkt	aistras	6
aistra	dkt	aistr�	1
aistra	dkt	aistroms	2
aistra	dkt	aistros	19
aistra	dkt	aistr�	8

are presented in the frequency dictionary as:

2452. aistra  (dkt.)		54
aistros 19, aistra 15, aistr� 8, aistras 6,
 aistrai 3, aistroms 2, aistr� 1




						� 2009 Andrius Utka



