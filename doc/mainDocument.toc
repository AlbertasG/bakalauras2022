\babel@toc {lithuanian}{}
\contentsline {section}{Santrauka}{3}{section*.1}%
\contentsline {section}{Summary}{4}{section*.2}%
\contentsline {section}{\nameref {sec:intro}}{5}{section*.3}%
\contentsline {section}{\numberline {1}Balso sintezavimas}{6}{section.1}%
\contentsline {subsection}{\numberline {1.1}Balso sintezatorius}{7}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Skirtumai tarp skirting\IeC {\k u} kalb\IeC {\k u} sintezatori\IeC {\k u}}{7}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Lietuvi\IeC {\k u} kalbos sintezatoriai}{8}{subsection.1.3}%
\contentsline {section}{\numberline {2}Teksto paruo\IeC {\v s}imas ir semantin\IeC {\.e}s klas\IeC {\.e}s}{10}{section.2}%
\contentsline {section}{\numberline {3}Teksto paruo\IeC {\v s}imo metodai ir algoritmai}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Reguliariosios i\IeC {\v s}rai\IeC {\v s}kos}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Grupavimas ir registras}{13}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}\IeC {\v Z}od\IeC {\v z}i\IeC {\k u} rinkiniai}{13}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Nestandartini\IeC {\k u} \IeC {\v z}od\IeC {\v z}i\IeC {\k u} su\IeC {\v z}ym\IeC {\.e}jimas}{14}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Algoritm\IeC {\k u} vertinimo kriterijai}{14}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Nestandartini\IeC {\k u} \IeC {\v z}od\IeC {\v z}i\IeC {\k u} aptikimas}{14}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Nestandartini\IeC {\k u} \IeC {\v z}od\IeC {\v z}i\IeC {\k u} semantinis klasifikavimas}{14}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Nestandartini\IeC {\k u} \IeC {\v z}od\IeC {\v z}i\IeC {\k u} i\IeC {\v s}skleidimas}{15}{subsubsection.3.5.3}%
\contentsline {section}{\numberline {4}Teksto paruo\IeC {\v s}imo algoritmo k\IeC {\=u}rimas VLE}{16}{section.4}%
\contentsline {subsection}{\numberline {4.1}Pirminis teksto sutvarkymas ir apdorojimas}{16}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Rankinis nestandartini\IeC {\k u} \IeC {\v z}od\IeC {\v z}i\IeC {\k u} i\IeC {\v s}rinkimas ir klasifikavimas}{17}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Trumpini\IeC {\k u} \IeC {\v z}odynas}{18}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Teksto paruo\IeC {\v s}imo algoritmai}{18}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Trumpiniai}{18}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Am\IeC {\v z}iai}{18}{subsubsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.3}Rom\IeC {\.e}ni\IeC {\v s}ki skai\IeC {\v c}iai}{18}{subsubsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.4}Datos}{19}{subsubsection.4.4.4}%
\contentsline {subsubsection}{\numberline {4.4.5}Skai\IeC {\v c}iai}{19}{subsubsection.4.4.5}%
\contentsline {subsubsection}{\numberline {4.4.6}Matavimo vienetai}{19}{subsubsection.4.4.6}%
\contentsline {subsubsection}{\numberline {4.4.7}Specialieji simboliai}{19}{subsubsection.4.4.7}%
\contentsline {section}{\numberline {5}Algoritmo \IeC {\k i}vertinimas ir rezultatas}{20}{section.5}%
\contentsline {section}{\nameref {sec:conclu}}{21}{section*.11}%
\contentsline {section}{\nameref {sec:future}}{22}{section*.12}%
\contentsline {section}{Literat\IeC {\=u}ros \IeC {\v s}altiniai}{23}{section*.13}%
