// regex_example1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <iterator>
#include <string>
#include <regex>

#define rulescount 771 //584//451

struct Taisykles { const char *KP; const char *DP; }
Tais[rulescount] = {
//	/*00*/{ "(\d?)(\d?)(\d?)(\d?)(\d?)(\d?)(\d?)(\d)(\d\d\d\d\d\d)", "$1 $2 $3 $4 $5 $6 $7 $8 $9" }, ???
//	{ "\\bjis\\. ji\\?\\b", "radau" },

	{ "  ", " " }, //NAV du tarpus i viena
	{ "(\\d)(\\d)(\\d)(\\d)(\\d)", "$1 $2 $3 $4 $5" }, //NAV pasto indeksai
	{ "^[�:;=&-]", "" }, //NAV \x1B
//	{ "", "" }, //NAV \x1B
	{ "tn=address", "" }, //NAV
	{ "tn=normal", "" }, //NAV
	{ "\\\\", "" }, //NAV

//	{ "� ", "" }, //NAV
	{ "Apyl.", "Apylink�s" }, //NAV
	{ "r\\. ([a-z])", "e`r $1" }, //NAV \.
	{ "SPEXSS", "eS-P�-E-iks-eS-e`S" }, //NAV
	{ "CCI", "C�-C�-I`" }, //NAV
	{ "Ulica", "Uly~ca" }, //NAV
	{ "Strasse", "�tra~se" }, //NAV
	{ "Strada", "Stra~da" }, //NAV
	{ "Avenue", "Aveniu`" }, //NAV
	{ "A[:;=-]([AES])", "$1" }, //NAV

	{ "\xA0", " " }, //keiciam nbsp i tarpeli
	{ "([a-zA-Z������������������])(\\d)", "$1 $2" }, //idedam tarpeli tarp raides ir skaiciaus
	{ "(\\d)([a-zA-Z������������������])", "$1 $2" }, //idedam tarpeli tarp skaiciaus ir raides NAV
//	{ "([a-zA-Z])(\\d)", "$1 $2" }, //idedam tarpeli tarp raides ir skaiciaus

	{ "\\bA([ ,]|$)", "A~$1" }, //NAV keliu kodai
	{ "\\bE([ ,]|$)", "E~$1" }, //NAV
	{ " e ", " e~ " }, //NAV
	{ " er ", " er~ " }, //NAV

	{ "\\b([Aa]not|[Aa]nt|[Aa]rti|[Aa]uk��iau|[Bb]e|[Dd]�ka|[Dd]�l|[Dd]�lei|[Gg]reta|[Ii]ki) (\\d)", "$1 #DgsK# $2" },
	{ "\\b([Ll]ig|[Ll]igi|[Ii]s|[Nn]etoli|[Nn]uo|[Pp]asak|[Pp]irmiau|[Pp]irm) (\\d)", "$1 #DgsK# $2" }, //NAV [Ll]ink|[Ll]inkui|
	{ "\\b([Pp]rie|[Pp]usiau|[��]alia|[Tt]arp|[Tt]oliau|[��]emiau|[Vv]idury|[Vv]idur|[Vv]ietoj|[Vv]ir�) (\\d)", "$1 #DgsK# $2" },
	{ "\\b([Vv]ir�um|[Vv]ir�uj|[Ii]�ilgai|[��]stri�ai|[��]kypai|[Ss]kersai|[Kk]iaurai|[Ss]krad�iai|[Aa]bipus|[Aa]napus|[��]iapus|[Aa]bigaliai|[Ii]� po|[Ii]� u�) (\\d)", "$1 #DgsK# $2" },
	{ "\\b([Aa]pie|[Aa]plink|[Aa]plinkui|[��]|[Pp]agal|[Pp]alei|[Pp]as|[Pp]askui|[Pp]askum|[Pp]er|[Pp]rie�|[Pp]rie�ais|[Pp]pro) (\\d)", "$1 #DgsG# $2" },
	{ "\\b([Ss]u|[Ss]ulig|[Tt]ies) (\\d)", "$1 #DgsI# $2" },

	{ "\\b([Uu]�) (\\d+) (min|val|s|sek|ms|h|d|m�n|mm|cm|dm|m|km|ft|yd|mi)\\b", "$1 #DgsK# $2 $3" }, // Kilm (laikas, atstumas ) Gal (pinigai)
	{ "\\b([Uu]�) (\\d+)\\b", "$1 #DgsG# $2" },
	{ "\\b([Pp]o) (\\d+) (min|val|s|sek|ms|h|d|m�n)\\b", "$1 #DgsK# $2 $3" }, // Kilm (laikas) Gal (kitais atv) Inag
	{ "\\b([Pp]o) (\\d+)\\b", "$1 #DgsG# $2" },

	{ "\\b([Aa]rti|[Ii]ki|[Ll]igi|[Nn]uo|[Tt]arp|[Vv]ietoj|[Uu]�|[Pp]o) (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io)\\b", "$1 #DgsK# $2" }, //Nuo sausio 21 d.

//	{ "(#Dgs[KGI]#) (\\d{1,12})(-[a-z��������9�]+\\b)", "$2 $3" }, //ismetam #Dgs[KGI]#, jei yra tipo 1-�

	{ "(#Dgs[KGI]#) ([12]\\d\\d\\d) m\\. (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) (\\d?\\d) d\\.",
	"$2-� met� $3 $1 $4 #MK# #VnsV# d." },
	{ "\\b([12]\\d\\d\\d) m\\. (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) (\\d?\\d) d\\.",
	"$1-� met� $2 $3 #MK# #VnsV# d." },

// nepilnos datos
	{ "(#Dgs[KGI]#) ([12]\\d\\d\\d) m\\. (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) m�n\\.",
	"$2-� met� $3 $1 #VnsV# m�n." },
	{ "\\b([12]\\d\\d\\d) m\\. (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) m�n\\.",
	"$1-� met� $2 #VnsV# m�n." },

	{ "\\b([12]\\d\\d\\d) m\\. (sausis|vasaris|kovas|balandis|gegu��|bir�elis|liepa|rugpj�tis|rugs�jis|spalis|lapkritis|gruodis)",
	"$1-� met� $2" },

	{ "(#Dgs[KGI]#) (sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) (\\d?\\d) d\\.",
	"$2 $1 $3 #MK# #VnsV# d." },
	{ "\\b(sausio|vasario|kovo|baland�io|gegu��s|bir�elio|liepos|rugpj��io|rugs�jo|spalio|lapkri�io|gruod�io) (\\d?\\d) d\\.",
	"$1 $2 #MK# #VnsV# d." },

// trumpos datos
	{ "(#Dgs[KGI]#) ([12]\\d\\d\\d)-(0[1-9]|10|11|12)-([0-3]\\d)\\b", "$2-� met� #$3m�n# $1 $4 #MK# #VnsV# d." },
	{ "\\b([12]\\d\\d\\d)-(0[1-9]|10|11|12)-([0-3]\\d)\\b", "$1-� met� #$2m�n# $3 #MK# #VnsV# d." },
	{ "(#Dgs[KGI]#) ([12]\\d\\d\\d) (0[1-9]|10|11|12) ([0-3]\\d)\\b", "$2-� met� #$3m�n# $1 $4 #MK# #VnsV# d." },
	{ "\\b([12]\\d\\d\\d) (0[1-9]|10|11|12) ([0-3]\\d)\\b", "$1-� met� #$2m�n# $3 #MK# #VnsV# d." },
	{ "#01m�n#", "sausio"},
	{ "#02m�n#", "vasario" },
	{ "#03m�n#", "kovo" },
	{ "#04m�n#", "baland�io" },
	{ "#05m�n#", "gegu��s" },
	{ "#06m�n#", "bir�elio" },
	{ "#07m�n#", "liepos" },
	{ "#08m�n#", "rugpj��io" },
	{ "#09m�n#", "rugs�jo" },
	{ "#10m�n#", "spalio" },
	{ "#11m�n#", "lapkri�io" },
	{ "#12m�n#", "gruod�io" },

// laiko intervalas //Galioja nurodytu laiku : 6 : 00 - 19 : 00
	{ "Galioja nurodytu laiku: (\\d?\\d):(\\d\\d)-(\\d?\\d):(\\d\\d)", "Galioja nurodytu laiku: nuo #DgsK# $1 #MotG# iki #DgsK# $3 #MotG#" },


// Datos 1941 m. - pirmais metais.
	{ "(#DgsK#) ([12]\\d\\d\\d) m\\. m\\.", "$2-� mokslo met�" },
	{ "(#DgsG#) ([12]\\d\\d\\d) m\\. m\\.", "$2-us mokslo metus" },
	{ "(#DgsI#) ([12]\\d\\d\\d) m\\. m\\.", "$2-ais mokslo metais" },
	{ "\\b([12]\\d\\d\\d) m\\. m\\.", "$1-ais mokslo metais" },		//inagininkas

	{ "(#DgsK#) ([12]\\d\\d\\d) m\\.", "$2-� met�" },
	{ "(#DgsG#) ([12]\\d\\d\\d) m\\.", "$2-us metus" },
	{ "(#DgsI#) ([12]\\d\\d\\d) m\\.", "$2-ais metais" },
	{ "\\b([12]\\d\\d\\d) m\\.", "$1-ais metais" },		//inagininkas

// Rom�ni�ki skai�iai
	{ "(^|\\n)I[\\)\\.][ \\t](.{2,30})(\\nII[\\)\\.][ \\t])", "$1Pirmas. $2$3" }, //ne toliau kaip u� 30 simboli� turi b�ti II
	{ "\\nII[\\)\\.][ \\t]", "\nAntras. " },
	{ "\\nIII[\\)\\.][ \\t]", "\nTre�ias. " },
	{ "(\\nIV[\\)\\.][ \\t])(.{2,30})\\nV[\\)\\.][ \\t]", "$1$2\nPenktas. " }, //sukeiciam IV ir V, nes negalima pirmiau pakeisti IV
	{ "\\nIV[\\)\\.][ \\t]", "\nKetvirtas. " },
	{ "\\nVI[\\)\\.][ \\t]", "\n�e�tas. " },
	{ "\\nVII[\\)\\.][ \\t]", "\nSeptintas. " },
	{ "\\nVIII[\\)\\.][ \\t]", "\nA�tuntas. " },
	{ "\\nIX[\\)\\.][ \\t]", "\nDevintas. " },
	{ "\\nX[\\)\\.][ \\t]", "\nDe�imtas. " },
	{ "\\nXI[\\)\\.][ \\t]", "\nVienuoliktas. " },
	{ "\\nXII[\\)\\.][ \\t]", "\nDvyliktas. " },
	{ "\\nXIII[\\)\\.][ \\t]", "\nTryliktas. " },
	{ "\\nXIV[\\)\\.][ \\t]", "\nKeturioliktas. " },
	{ "\\nXV[\\)\\.][ \\t]", "\nPenkioliktas. " },
	{ "\\nXVI[\\)\\.][ \\t]", "\n�e�ioliktas. " },
	{ "\\nXVII[\\)\\.][ \\t]", "\nSeptynioliktas. " },
	{ "\\nXVIII[\\)\\.][ \\t]", "\nA�tuonioliktas. " },
	{ "\\nXIX[\\)\\.][ \\t]", "\nDevynioliktas. " },
	{ "\\nXX[\\)\\.][ \\t]", "\nDvide�imtas. " },
	{ "\\nXXI[\\)\\.][ \\t]", "\nDvide�imt pirmas. " },
	{ "\\nXXII[\\)\\.][ \\t]", "\nDvide�imt antras. " },
	{ "\\nXXIII[\\)\\.][ \\t]", "\nDvide�imt tre�ias. " },
	{ "\\nXXIV[\\)\\.][ \\t]", "\nDvide�imt ketvirtas. " },
	{ "\\nXXV[\\)\\.][ \\t]", "\nDvide�imt penktas. " },
	{ "\\nXXVI[\\)\\.][ \\t]", "\nDvide�imt �e�tas. " },
	{ "\\nXXVII[\\)\\.][ \\t]", "\nDvide�imt septintas. " },
	{ "\\nXXVII[\\)\\.][ \\t]", "\nDvide�imt a�tuntas. " },
	{ "\\nXXIX[\\)\\.][ \\t]", "\nDvide�imt devintas. " },
	{ "\\nXXX[\\)\\.][ \\t]", "\nTrisde�imtas. " },

	{ "\\ba. pr\\.", "a. prad�ioje" },
	{ "\\ba. pab\\.", "a. pabaigoje" },
	{ "\\ba. vid\\.", "a. viduryje" },

// Amziai
	{ "\\b(I a.) ([prad�ioje|pabaigoje|viduryje])", "pirmo am�iaus $2" },
	{ "\\bI a.", "pirmame am�iuje" },
	{ "\\b(II a.) ([prad�ioje|pabaigoje|viduryje])", "antro am�iaus $2" },
	{ "\\bII a.", "antrame am�iuje" },
	{ "\\b(III a.) ([prad�ioje|pabaigoje|viduryje])", "tre�io am�iaus $2" },
	{ "\\bIII a.", "tre�iame am�iuje" },
	{ "\\b(IV a.) ([prad�ioje|pabaigoje|viduryje])", "ketvirto am�iaus $2" },
	{ "\\bIV a.", "ketvirtame am�iuje" },
	{ "\\b(V a.) ([prad�ioje|pabaigoje|viduryje])", "penkto am�iaus $2" },
	{ "\\bV a.", "penktame am�iuje" },
	{ "\\b(VI a.) ([prad�ioje|pabaigoje|viduryje])", "�e�to am�iaus $2" },
	{ "\\bVI a.", "�e�tame am�iuje" },
	{ "\\b(VII a.) ([prad�ioje|pabaigoje|viduryje])", "septinto am�iaus $2" },
	{ "\\bVII a.", "septintame am�iuje" },
	{ "\\b(VIII a.) ([prad�ioje|pabaigoje|viduryje])", "a�tunto am�iaus $2" },
	{ "\\bVIII a.", "a�tuntame am�iuje" },
	{ "\\b(IX a.) ([prad�ioje|pabaigoje|viduryje])", "devinto am�iaus $2" },
	{ "\\bIX a.", "devintame am�iuje" },
	{ "\\b(X a.) ([prad�ioje|pabaigoje|viduryje])", "de�imto am�iaus $2" },
	{ "\\bX a.", "de�imtame am�iuje" },
	{ "\\b(XI a.) ([prad�ioje|pabaigoje|viduryje])", "vienuolikto am�iaus $2" },
	{ "\\bXI a.", "vienuoliktame am�iuje" },
	{ "\\b(XII a.) ([prad�ioje|pabaigoje|viduryje])", "dvylikto am�iaus $2" },
	{ "\\bXII a.", "dvyliktame am�iuje" },
	{ "\\b(XIII a.) ([prad�ioje|pabaigoje|viduryje])", "trylikto am�iaus $2" },
	{ "\\bXIII a.", "tryliktame am�iuje" },
	{ "\\b(XIV a.) ([prad�ioje|pabaigoje|viduryje])", "keturiolikto am�iaus $2" },
	{ "\\bXIV a.", "keturioliktame am�iuje" },
	{ "\\b(XV a.) ([prad�ioje|pabaigoje|viduryje])", "penkiolikto am�iaus $2" },
	{ "\\bXV a.", "penkioliktame am�iuje" },
	{ "\\b(XVI a.) ([prad�ioje|pabaigoje|viduryje])", "�e�iolikto am�iaus $2" },
	{ "\\bXVI a.", "�e�ioliktame am�iuje" },
	{ "\\b(XVII a.) ([prad�ioje|pabaigoje|viduryje])", "septyniolikto am�iaus $2" },
	{ "\\bXVII a.", "septynioliktame am�iuje" },
	{ "\\b(XVIII a.) ([prad�ioje|pabaigoje|viduryje])", "a�tuoniolikto am�iaus $2" },
	{ "\\bXVIII a.", "a�tuonioliktame am�iuje" },
	{ "\\b(XIX a.) ([prad�ioje|pabaigoje|viduryje])", "devyniolikto am�iaus $2" },
	{ "\\bXIX a.", "devynioliktame am�iuje" },
	{ "\\b(XX a.) ([prad�ioje|pabaigoje|viduryje])", "dvide�imto am�iaus $2" },
	{ "\\bXX a.", "dvide�imtame am�iuje" },
	{ "\\b(XXI a.) ([prad�ioje|pabaigoje|viduryje])", "dvide�imt pirmo am�iaus $2" },
	{ "\\bXXI a.", "dvide�imt pirmame am�iuje" },


	{ "(#Dgs[KGI]#) (\\d+) (km) (\\d+) (m)\\b", "$1 $2 $3 $1 $4 $5" }, //perkeliam #Dgs[KGI]# per skaiciu ir matavimo vieneta
	{ "(#Dgs[KGI]#) (\\d+) (m) (\\d+) (cm)\\b", "$1 $2 $3 $1 $4 $5" },
	{ "(#Dgs[KGI]#) (\\d+) (val) (\\d+) (min)\\b", "$1 $2 $3 $1 $4 $5" },
	{ "(#Dgs[KGI]#) (\\d+) (min) (\\d+) (sek)\\b", "$1 $2 $3 $1 $4 $5" },

	{ "(\\d) (min|val|s|sek|ms|t|h|mph|mi/h|kWh|mi|ft)\\b", "$1 #MotG# $2" },

	{ "(\\d{1,3} km \\d{1,3} m toliau)\\b", "#DgsI# $1" }, //NAV 2 km 100 m toliau
	{ "(\\d{1,3} k?m toliau)\\b", "#DgsI# $1" }, //NAV 100 m toliau
	{ "(\\d{1,3} km \\d{1,3} m trumpiau)\\b", "#DgsI# $1" }, //NAV 2 km 100 m trumpiau
	{ "(\\d{1,3} k?m trumpiau)\\b", "#DgsI# $1" }, //NAV 100 m trumpiau

//	{ "(\\d{1,3}) (toliau)\\b", "#DgsI# $1 #VK# $2" }, //NAV A 1 toliau

	{ "(#Dgs[KGI]#) (\\d{1,12})\\b", "$1 $2 $1" }, //perkeliam #Dgs[KGI]# per visa skaiciu

	{ "(#Dgs[KGI]#) (#MotG#)", "$2 $1" },
	{ "(#Dgs[KGI]#) (#MK#)", "$2 $1" },
//	{ "(#Dgs[KGI]#) (#VK#)", "$2 $1" }, //NAV


	{ "(000)(\\d{9})\\b", "$2" },
	{ "(#Dgs[KGI]#) (00|\\D)1(\\d{9})\\b", "$1 #VnsV# $1 mlrd $1 $3" },
	{ "(00|\\D)1(\\d{9})\\b", "#VnsV# mlrd $2" },
	{ "(#Dgs[KGI]#) (\\d{1,3})(\\d{9})\\b", "$1 $2 $1 mlrd $1 $3" },
	{ "(\\d{1,3})(\\d{9})\\b", "$1 mlrd $2" },
	{ "(000)(\\d{6})\\b", "$2" },
	{ "(#Dgs[KGI]#) (00|\\D)1(\\d{6})\\b", "$1 #VnsV# $1 mln $1 $3" },
	{ "(00|\\D)1(\\d{6})\\b", "#VnsV# mln $2" },
	{ "(#Dgs[KGI]#) (\\d{1,3})(\\d{6})\\b", "$1 $2 $1 mln $1 $3" },
	{ "(\\d{1,3})(\\d{6})\\b", "$1 mln $2" },
	{ "(000)(\\d{3})\\b", "$2" },
	{ "(#Dgs[KGI]#) (00|\\b)1(\\d{3})\\b", "$1 #VnsV# $1 tukst $1 $3" },
	{ "(00|\\b)1(\\d{3})\\b", "#VnsV# tukst $2" },								//\\D->\\b
	{ "(#Dgs[KGI]#) (\\d{1,3})(\\d{3})\\b", "$1 $2 $1 tukst $1 $3" },
	{ "(\\d{1,3})(\\d{3})\\b", "$1 tukst $2" },

	{ "(1\\d)\\b", "$1 #DgsK#" },
	{ "(\\d0)\\b", "$1 #DgsK#" },
	{ "([02-9]1)\\b", "$1 #VnsV#" },
	{ "\\b1 ([^0-9])", "1 #VnsV# $1" }, //NAV 1 min
	{ "\\b0 ([^0-9])", "0 #DgsK# $1" }, //NAV 0 laipsniu
		// else #DgsV#

	{ "(#Vns[KGI]#) (#MotG#)", "$2 $1" }, //NAV 1 min
	{ "(#Vns[KGI]#) (#MK#)", "$2 $1" },   //NAV 1 min
//	{ "(#Vns[KGI]#) (#VK#)", "$2 $1" },   //NAV 1 min

	{ " #DgsK#-", "-" },	//is karto ismetu, jei iterpiau pries bruksneli 
	{ " #VnsV#-", "-" },

	{ "(#DgsK#|#VnsV#) (#MotG#)", "$2 $1" },
	{ "(#DgsK#|#VnsV#) (#MK#)", "$2 $1" },
//	{ "(#DgsK#|#VnsV#) (#VK#)", "$2 $1" }, //NAV

	{ "#DgsK# #Dgs[KGI]#", "#DgsK#" }, //perkeliau
	{ "#VnsV# #Dgs([KGI]#)", "#Vns$1" },
	{ "#Vns([KGI]#) #VnsV#", "#Vns$1" }, //new
	{ "#Dgs([KGI]#) #VnsV#", "#Vns$1" }, //new

	{ "0(\\d\\d)\\b", "$1" },
	{ "(#DgsK#) 100\\b", "�imto $1" },
	{ "(#DgsG#) 100\\b", "�imt� $1" },
	{ "(#DgsI#) 100\\b", "�imtu $1" },
	{ "100\\b", "�imtas" },
	{ "(#DgsK#) 1(\\d\\d)\\b", "�imto $1 $2" },
	{ "(#DgsG#) 1(\\d\\d)\\b", "�imt� $1 $2" },
	{ "(#DgsI#) 1(\\d\\d)\\b", "�imtu $1 $2" },
	{ "1(\\d\\d)\\b", "�imtas $1" },
	{ "(#DgsK#) (\\d)00\\b", "$1 $2 �imt�" },
	{ "(#DgsG#) (\\d)00\\b", "$1 $2 �imtus" },
	{ "(#DgsI#) (\\d)00\\b", "$1 $2 �imtais" },
	{ "(\\d)00\\b", "$1 �imtai" },
	{ "(#DgsK#) (\\d)(\\d\\d)\\b", "$1 $2 �imt� $1 $3" },
	{ "(#DgsG#) (\\d)(\\d\\d)\\b", "$1 $2 �imtus $1 $3" },
	{ "(#DgsI#) (\\d)(\\d\\d)\\b", "$1 $2 �imtais $1 $3" },
	{ "(\\d)(\\d\\d)\\b", "$1 �imtai $2" },

	{ "#DgsK# 10 #MK#", "de�imtos" },
	{ "#DgsG# 10 #MK#", "de�imt�" },
	{ "#DgsI# 10 #MK#", "de�imta" },
	{ "\\b10 #MK#", "de�imta" },
	{ "#DgsK# 11 #MK#", "vienuoliktos" },
	{ "#DgsG# 11 #MK#", "vienuolikt�" },
	{ "#DgsI# 11 #MK#", "vienuolikta" },
	{ "\\b11 #MK#", "vienuolikta" },			//nepanaudotus #Dgs[GI]# pasalinsim veliau ????????
	{ "#DgsK# 12 #MK#", "dvyliktos" },
	{ "#DgsG# 12 #MK#", "dvylikt�" },
	{ "#DgsI# 12 #MK#", "dvylikta" },
	{ "\\b12 #MK#", "dvylikta" },
	{ "#DgsK# 13 #MK#", "tryliktos" },
	{ "#DgsG# 13 #MK#", "trylikt�" },
	{ "#DgsI# 13 #MK#", "trylikta" },
	{ "\\b13 #MK#", "trylikta" },
	{ "#DgsK# 14 #MK#", "keturioliktos" },
	{ "#DgsG# 14 #MK#", "keturiolikt�" },
	{ "#DgsI# 14 #MK#", "keturiolikta" },
	{ "\\b14 #MK#", "keturiolikta" },
	{ "#DgsK# 15 #MK#", "penkioliktos" },
	{ "#DgsG# 15 #MK#", "penkiolikt�" },
	{ "#DgsI# 15 #MK#", "penkiolikta" },
	{ "\\b15 #MK#", "penkiolikta" },
	{ "#DgsK# 16 #MK#", "�e�ioliktos" },
	{ "#DgsG# 16 #MK#", "�e�iolikt�" },
	{ "#DgsI# 16 #MK#", "�e�iolikta" },
	{ "\\b16 #MK#", "�e�iolikta" },
	{ "#DgsK# 17 #MK#", "septynioliktos" },
	{ "#DgsG# 17 #MK#", "septyniolikt�" },
	{ "#DgsI# 17 #MK#", "septyniolikta" },
	{ "\\b17 #MK#", "septyniolikta" },
	{ "#DgsK# 18 #MK#", "a�tuonioliktos" },
	{ "#DgsG# 18 #MK#", "a�tuoniolikt�" },
	{ "#DgsI# 18 #MK#", "a�tuoniolikta" },
	{ "\\b18 #MK#", "a�tuoniolikta" },
	{ "#DgsK# 19 #MK#", "devynioliktos" },
	{ "#DgsG# 19 #MK#", "devyniolikt�" },
	{ "#DgsI# 19 #MK#", "devyniolikta" },
	{ "\\b19 #MK#", "devyniolikta" },
	{ "#DgsK# 20 #MK#", "dvide�imtos" }, 
	{ "#DgsG# 20 #MK#", "dvide�imt�" },
	{ "#DgsI# 20 #MK#", "dvide�imta" },
	{ "\\b20 #MK#", "dvide�imta" },
	{ "#DgsK# 30 #MK#", "trisde�imtos" },
	{ "#DgsG# 30 #MK#", "trisde�imt�" },
	{ "#DgsI# 30 #MK#", "trisde�imta" },
	{ "\\b30 #MK#", "trisde�imta" },

	{ "(\\d{1,2}) (#VnsV#|#DgsK#) (toliau)\\b", "#DgsI# $1 #VK# $3" }, //NAV A 1 toliau
	{ "(\\d{1,2}) (toliau)\\b", "#DgsI# $1 #VK# $2" }, //NAV A 2 toliau

	{ "#DgsK# 10 #VK#", "de�imto" },
	{ "#DgsG# 10 #VK#", "de�imt�" },
	{ "#DgsI# 10 #VK#", "de�imtu" },
	{ "\\b10 #VK#", "de�imtas" },
	{ "#DgsK# 11 #VK#", "vienuolikto" },
	{ "#DgsG# 11 #VK#", "vienuolikt�" },
	{ "#DgsI# 11 #VK#", "vienuoliktu" },
	{ "\\b11 #VK#", "vienuoliktas" },			//nepanaudotus #Dgs[GI]# pasalinsim veliau ????????
	{ "#DgsK# 12 #VK#", "dvylikto" },
	{ "#DgsG# 12 #VK#", "dvylikt�" },
	{ "#DgsI# 12 #VK#", "dvyliktu" },
	{ "\\b12 #VK#", "dvyliktas" },
	{ "#DgsK# 13 #VK#", "trylikto" },
	{ "#DgsG# 13 #VK#", "trylikt�" },
	{ "#DgsI# 13 #VK#", "tryliktu" },
	{ "\\b13 #VK#", "tryliktas" },
	{ "#DgsK# 14 #VK#", "keturiolikto" },
	{ "#DgsG# 14 #VK#", "keturiolikt�" },
	{ "#DgsI# 14 #VK#", "keturioliktu" },
	{ "\\b14 #VK#", "keturioliktas" },
	{ "#DgsK# 15 #VK#", "penkiolikto" },
	{ "#DgsG# 15 #VK#", "penkiolikt�" },
	{ "#DgsI# 15 #VK#", "penkioliktu" },
	{ "\\b15 #VK#", "penkioliktas" },
	{ "#DgsK# 16 #VK#", "�e�iolikto" },
	{ "#DgsG# 16 #VK#", "�e�iolikt�" },
	{ "#DgsI# 16 #VK#", "�e�ioliktu" },
	{ "\\b16 #VK#", "�e�ioliktas" },
	{ "#DgsK# 17 #VK#", "septyniolikto" },
	{ "#DgsG# 17 #VK#", "septyniolikt�" },
	{ "#DgsI# 17 #VK#", "septynioliktu" },
	{ "\\b17 #VK#", "septynioliktas" },
	{ "#DgsK# 18 #VK#", "a�tuoniolikto" },
	{ "#DgsG# 18 #VK#", "a�tuoniolikt�" },
	{ "#DgsI# 18 #VK#", "a�tuonioliktu" },
	{ "\\b18 #VK#", "a�tuonioliktas" },
	{ "#DgsK# 19 #VK#", "devyniolikto" },
	{ "#DgsG# 19 #VK#", "devyniolikt�" },
	{ "#DgsI# 19 #VK#", "devynioliktu" },
	{ "\\b19 #VK#", "devynioliktas" },
	{ "#DgsK# 20 #VK#", "dvide�imto" },
	{ "#DgsG# 20 #VK#", "dvide�imt�" },
	{ "#DgsI# 20 #VK#", "dvide�imtu" },
	{ "\\b20 #VK#", "dvide�imtas" },
	{ "#DgsK# 30 #VK#", "trisde�imto" },
	{ "#DgsG# 30 #VK#", "trisde�imt�" },
	{ "#DgsI# 30 #VK#", "trisde�imtu" },
	{ "\\b30 #VK#", "trisde�imtas" },

	{ "0(\\d)\\b", "$1" },
	{ "#DgsK# 10\\b", "de�imties" },
	{ "#DgsI# 10\\b", "de�im�ia" },
	{ "10\\b", "de�imt" },
	{ "#DgsK# 11\\b", "vienuolikos" },
	{ "11\\b", "vienuolika" },			//nepanaudotus #Dgs[GI]# pasalinsim veliau
	{ "#DgsK# 12\\b", "dvylikos" },
	{ "12\\b", "dvylika" },
	{ "#DgsK# 13\\b", "trylikos" },
	{ "13\\b", "trylika" },
	{ "#DgsK# 14\\b", "keturiolikos" },
	{ "14\\b", "keturiolika" },
	{ "#DgsK# 15\\b", "penkiolikos" },
	{ "15\\b", "penkiolika" },
	{ "#DgsK# 16\\b", "�e�iolikos" },
	{ "16\\b", "�e�iolika" },
	{ "#DgsK# 17\\b", "septyniolikos" },
	{ "17\\b", "septyniolika" },
	{ "#DgsK# 18\\b", "a�tuoniolikos" },
	{ "18\\b", "a�tuoniolika" },
	{ "#DgsK# 19\\b", "devyniolikos" },
	{ "19\\b", "devyniolika" },

	{ "#DgsK# 20\\b", "dvide�imties" }, 			//nepanaudotus #DgsG# pasalinsim veliau
	{ "#DgsI# 20\\b", "dvide�im�ia" },
	{ "20\\b", "dvide�imt" },
	{ "#DgsK# 30\\b", "trisde�imties" },
	{ "#DgsI# 30\\b", "trisde�im�ia" },
	{ "30\\b", "trisde�imt" },
	{ "#DgsK# 40\\b", "keturiasde�imties" },
	{ "#DgsI# 40\\b", "keturiasde�im�ia" },
	{ "40\\b", "keturiasde�imt" },
	{ "#DgsK# 50\\b", "penkiasde�imties" },
	{ "#DgsI# 50\\b", "penkiasde�im�ia" },
	{ "50\\b", "penkiasde�imt" },
	{ "#DgsK# 60\\b", "�e�iasde�imties" },
	{ "#DgsI# 60\\b", "�e�iasde�im�ia" },
	{ "60\\b", "�e�iasde�imt" },
	{ "#DgsK# 70\\b", "septyniasde�imties" },
	{ "#DgsI# 70\\b", "septyniasde�im�ia" },
	{ "70\\b", "septyniasde�imt" },
	{ "#DgsK# 80\\b", "a�tuoniasde�imties" },
	{ "#DgsI# 80\\b", "a�tuoniasde�im�ia" },
	{ "80\\b", "a�tuoniasde�imt" },
	{ "#DgsK# 90\\b", "devyniasde�imties" },
	{ "#DgsI# 90\\b", "devyniasde�im�ia" },
	{ "90\\b", "devyniasde�imt" },
	{ "(#Dgs[KGI]#) 2([1-9])\\b", "dvide�imt $1 $2" },
	{ "2([1-9])\\b", "dvide�imt $1" },
	{ "(#Dgs[KGI]#) 3([1-9])\\b", "trisde�imt $1 $2" },
	{ "3([1-9])\\b", "trisde�imt $1" },
	{ "(#Dgs[KGI]#) 4([1-9])\\b", "keturiasde�imt $1 $2" },
	{ "4([1-9])\\b", "keturiasde�imt $1" },
	{ "(#Dgs[KGI]#) 5([1-9])\\b", "penkiasde�imt $1 $2" },
	{ "5([1-9])\\b", "penkiasde�imt $1" },
	{ "(#Dgs[KGI]#) 6([1-9])\\b", "�e�iasde�imt $1 $2" },
	{ "6([1-9])\\b", "�e�iasde�imt $1" },
	{ "(#Dgs[KGI]#) 7([1-9])\\b", "septyniasde�imt $1 $2" },
	{ "7([1-9])\\b", "septyniasde�imt $1" },
	{ "(#Dgs[KGI]#) 8([1-9])\\b", "a�tuoniasde�imt $1 $2" },
	{ "8([1-9])\\b", "a�tuoniasde�imt $1" },
	{ "(#Dgs[KGI]#) 9([1-9])\\b", "devyniasde�imt $1 $2" },
	{ "9([1-9])\\b", "devyniasde�imt $1" },

	{ "#DgsK# 0{1,3}\\b", "nulio" },
	{ "#DgsG# 0{1,3}\\b", "nul�" },
	{ "#DgsI# 0{1,3}\\b", "nuliu" },
	{ "0{1,3}\\b", "nulis" },

	{ "#DgsK# 1 #MotG#", "vienos" },
	{ "#DgsG# 1 #MotG#", "vien�" },
	{ "#DgsI# 1 #MotG#", "viena" },
	{ "1 #MotG#", "viena" },
	{ "#DgsK# 2 #MotG#", "dviej�" },
	{ "#DgsI# 2 #MotG#", "dviem" },
	{ "2 #MotG#", "dvi" },
//	{ "#DgsK# 3 #MotG#", "trij�" }, =VyrG
//	{ "#DgsG# 3 #MotG#", "tris" },
//	{ "#DgsI# 3 #MotG#", "trimis" },
//	{ "3 #MotG#", "trys" },
	{ "#DgsK# 4 #MotG#", "keturi�" },
	{ "#DgsG# 4 #MotG#", "keturias" },
	{ "#DgsI# 4 #MotG#", "keturiomis" },
	{ "4 #MotG#", "keturios" },
	{ "#DgsK# 5 #MotG#", "penki�" },
	{ "#DgsG# 5 #MotG#", "penkias" },
	{ "#DgsI# 5 #MotG#", "penkiomis" },
	{ "5 #MotG#", "penkios" },
	{ "#DgsK# 6 #MotG#", "�e�i�" },
	{ "#DgsG# 6 #MotG#", "�e�ias" },
	{ "#DgsI# 6 #MotG#", "�e�iomis" },
	{ "6 #MotG#", "�e�ios" },
	{ "#DgsK# 7 #MotG#", "septyni�" },
	{ "#DgsG# 7 #MotG#", "septynias" },
	{ "#DgsI# 7 #MotG#", "septyniomis" },
	{ "7 #MotG#", "septynios" },
	{ "#DgsK# 8 #MotG#", "a�tuoni�" },
	{ "#DgsG# 8 #MotG#", "a�tuonias" },
	{ "#DgsI# 8 #MotG#", "a�tuonioms" },
	{ "8 #MotG#", "a�tuonios" },
	{ "#DgsK# 9 #MotG#", "devyni�" },
	{ "#DgsG# 9 #MotG#", "devynias" },
	{ "#DgsI# 9 #MotG#", "devyniomis" },
	{ "9 #MotG#", "devynios" },

	{ "#DgsK# 1 #MK#", "pirmos" },
	{ "#DgsG# 1 #MK#", "pirm�" },
	{ "#DgsI# 1 #MK#", "pirma" },
	{ "1 #MK#", "pirma" },
	{ "#DgsK# 2 #MK#", "antros" },
	{ "#DgsG# 1 #MK#", "antr�" },
	{ "#DgsI# 2 #MK#", "antra" },
	{ "2 #MK#", "antra" },
	{ "#DgsK# 3 #MK#", "tre�ios" },
	{ "#DgsG# 3 #MK#", "tre�i�" },
	{ "#DgsI# 3 #MK#", "tre�ia" },
	{ "3 #MK#", "tre�ia" },
	{ "#DgsK# 4 #MK#", "ketvirtos" },
	{ "#DgsG# 4 #MK#", "ketvirt�" },
	{ "#DgsI# 4 #MK#", "ketvirta" },
	{ "4 #MK#", "ketvirta" },
	{ "#DgsK# 5 #MK#", "penktos" },
	{ "#DgsG# 5 #MK#", "penkt�" },
	{ "#DgsI# 5 #MK#", "penkta" },
	{ "5 #MK#", "penkta" },
	{ "#DgsK# 6 #MK#", "�e�tos" },
	{ "#DgsG# 6 #MK#", "�e�t�" },
	{ "#DgsI# 6 #MK#", "�e�ta" },
	{ "6 #MK#", "�e�ta" },
	{ "#DgsK# 7 #MK#", "septintos" },
	{ "#DgsG# 7 #MK#", "septint�" },
	{ "#DgsI# 7 #MK#", "septinta" },
	{ "7 #MK#", "septinta" },
	{ "#DgsK# 8 #MK#", "a�tuntos" },
	{ "#DgsG# 8 #MK#", "a�tunt�" },
	{ "#DgsI# 8 #MK#", "a�tunta" },
	{ "8 #MK#", "a�tunta" },
	{ "#DgsK# 9 #MK#", "devintos" },
	{ "#DgsG# 9 #MK#", "devint�" },
	{ "#DgsI# 9 #MK#", "devinta" },
	{ "9 #MK#", "devinta" },

	{ "#DgsK# 1 #VK#", "pirmo" },
	{ "#DgsG# 1 #VK#", "pirm�" },
	{ "#DgsI# 1 #VK#", "pirmu" },
	{ "1 #VK#", "pirmas" },
	{ "#DgsK# 2 #VK#", "antro" },
	{ "#DgsG# 1 #VK#", "antr�" },
	{ "#DgsI# 2 #VK#", "antru" },
	{ "2 #VK#", "antras" },
	{ "#DgsK# 3 #VK#", "tre�io" },
	{ "#DgsG# 3 #VK#", "tre�i�" },
	{ "#DgsI# 3 #VK#", "tre�iu" },
	{ "3 #VK#", "tre�ias" },
	{ "#DgsK# 4 #VK#", "ketvirto" },
	{ "#DgsG# 4 #VK#", "ketvirt�" },
	{ "#DgsI# 4 #VK#", "ketvirtu" },
	{ "4 #VK#", "ketvirtas" },
	{ "#DgsK# 5 #VK#", "penkto" },
	{ "#DgsG# 5 #VK#", "penkt�" },
	{ "#DgsI# 5 #VK#", "penktu" },
	{ "5 #VK#", "penktas" },
	{ "#DgsK# 6 #VK#", "�e�to" },
	{ "#DgsG# 6 #VK#", "�e�t�" },
	{ "#DgsI# 6 #VK#", "�e�tu" },
	{ "6 #VK#", "�e�tas" },
	{ "#DgsK# 7 #VK#", "septinto" },
	{ "#DgsG# 7 #VK#", "septint�" },
	{ "#DgsI# 7 #VK#", "septintu" },
	{ "7 #VK#", "septintas" },
	{ "#DgsK# 8 #VK#", "a�tunto" },
	{ "#DgsG# 8 #VK#", "a�tunt�" },
	{ "#DgsI# 8 #VK#", "a�tuntu" },
	{ "8 #VK#", "a�tuntas" },
	{ "#DgsK# 9 #VK#", "devinto" },
	{ "#DgsG# 9 #VK#", "devint�" },
	{ "#DgsI# 9 #VK#", "devintu" },
	{ "9 #VK#", "devintas" },

	{ "#DgsK# 1\\b", "vieno" },
	{ "#DgsG# 1\\b", "vien�" },
	{ "#DgsI# 1\\b", "vienu" },
	{ "1\\b", "vienas" },
	{ "#DgsK# 2\\b", "dviej�" },
	{ "#DgsI# 2\\b", "dviem" },
	{ "2\\b", "du" },
	{ "#DgsK# 3\\b", "trij�" },
	{ "#DgsG# 3\\b", "tris" },
	{ "#DgsI# 3\\b", "trimis" },
	{ "3\\b", "trys" },
	{ "#DgsK# 4\\b", "keturi�" },
	{ "#DgsG# 4\\b", "keturis" },
	{ "#DgsI# 4\\b", "keturiais" },
	{ "4\\b", "keturi" },
	{ "#DgsK# 5\\b", "penki�" },
	{ "#DgsG# 5\\b", "penkis" },
	{ "#DgsI# 5\\b", "penkiais" },
	{ "5\\b", "penki" },
	{ "#DgsK# 6\\b", "�e�i�" },
	{ "#DgsG# 6\\b", "�e�is" },
	{ "#DgsI# 6\\b", "�e�iais" },
	{ "6\\b", "�e�i" },
	{ "#DgsK# 7\\b", "septyni�" },
	{ "#DgsG# 7\\b", "septynis" },
	{ "#DgsI# 7\\b", "septyniais" },
	{ "7\\b", "septyni" },
	{ "#DgsK# 8\\b", "a�tuoni�" },
	{ "#DgsG# 8\\b", "a�tuonis" },
	{ "#DgsI# 8\\b", "a�tuoniais" },
	{ "8\\b", "a�tuoni" },
	{ "#DgsK# 9\\b", "devyni�" },
	{ "#DgsG# 9\\b", "devynis" },
	{ "#DgsI# 9\\b", "devyniais" },
	{ "9\\b", "devyni" },

	{ "vienas-�j�", "pirm�j�" },	//analogiska failui skaitm.txt // galima b�t� "pirm�j� #DgsK#" !!! //pabaigoje negalima , nes baigiasi lietuviska raide � 
	{ "du-�j�", "antr�j�" },
	{ "trys-i?�j�", "tre�i�j�" },
	{ "keturi-�j�", "ketvirt�j�" },
	{ "penki-�j�", "penkt�j�" },
	{ "�e�i-�j�", "�e�t�j�" },
	{ "septyni-�j�", "septint�j�" },
	{ "a�tuoni-�j�", "a�tunt�j�" },
	{ "devyni-�j�", "devint�j�" },
	{ "de�imt-�j�", "de�imt�j�" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-(�j�)", "$1likt$2" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-�j�", "$1de�imt�j�" },
	{ "�imta[si]-�j�", "�imt�j�" },
	{ "t�kstantis-�j�", "t�kstant�j�" },
	{ "t�kstan�iai-�j�", "t�kstant�j�" },

	{ "vienas-�", "pirm�" },	//kilm analogiska failui skaitm.txt // galima b�t� "pirm� #DgsK#" !!! //pabaigoje negalima , nes baigiasi lietuviska raide � 
	{ "du-�", "antr�" },
	{ "trys-i?�", "tre�i�" },
	{ "keturi-�", "ketvirt�" },
	{ "penki-�", "penkt�" },
	{ "�e�i-�", "�e�t�" },
	{ "septyni-�", "septint�" },
	{ "a�tuoni-�", "a�tunt�" },
	{ "devyni-�", "devint�" },
	{ "de�imt-�", "de�imt�" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-(�)", "$1likt$2" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-�", "$1de�imt�" },
	{ "�imta[si]-�", "�imt�" },
	{ "t�kstantis-�", "t�kstant�" },
	{ "t�kstan�iai-�", "t�kstant�" },

	{ "vienas-us", "pirmus" }, //gal
	{ "du-us", "antr�" },
	{ "trys-i?us", "tre�ius" },
	{ "keturi-us", "ketvirtus" },
	{ "penki-us", "penktus" },
	{ "�e�i-us", "�e�tus" },
	{ "septyni-us", "septintus" },
	{ "a�tuoni-us", "a�tuntus" },
	{ "devyni-us", "devintus" },
	{ "de�imt-us", "de�imtus" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-(us)", "$1likt$2" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-us", "$1de�imtus" },
	{ "�imta[si]-us", "�imtus" },
	{ "t�kstantis-us", "t�kstantus" },
	{ "t�kstan�iai-us", "t�kstantus" },

	{ "vienas-ais", "pirmais" }, //inag
	{ "du-ais", "antrais" },
	{ "trys-i?ais", "tre�iais" },
	{ "keturi-ais", "ketvirtais" },
	{ "penki-ais", "penktais" },
	{ "�e�i-ais", "�e�tais" },
	{ "septyni-ais", "septintais" },
	{ "a�tuoni-ais", "a�tuntais" },
	{ "devyni-ais", "devintais" },
	{ "de�imt-ais", "de�imtais" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-(ais)", "$1likt$2" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-ais", "$1de�imtais" },
	{ "�imta[si]-ais", "�imtais" },
	{ "t�kstantis-ais", "t�kstantais" },
	{ "t�kstan�iai-ais", "t�kstantais" },

	{ "vienas-oji", "pirmoji" }, //ivardz mot vard NAV 15 eil 
	{ "du-oji", "antroji" },
	{ "trys-i?oji", "tre�ioji" },
	{ "keturi-oji", "ketvirtoji" },
	{ "penki-oji", "penktoji" },
	{ "�e�i-oji", "�e�toji" },
	{ "septyni-oji", "septintoji" },
	{ "a�tuoni-oji", "a�tuntoji" },
	{ "devyni-oji", "devintoji" },
	{ "de�imt-oji", "de�imtoji" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-(oji)", "$1likt$2" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-oji", "$1de�imtoji" },
	{ "�imta[si]-oji", "�imtoji" },
	{ "t�kstantis-oji", "t�kstantoji" },
	{ "t�kstan�iai-oji", "t�kstantoji" },

	{ "vienas-[oO]sios", "pirmosios" }, //ivardz mot kilm NAV 15 eil
	{ "du-[oO]sios", "antrosios" },
	{ "trys-[iI]?osios", "tre�iosios" },
	{ "keturi-[oO]sios", "ketvirtosios" },
	{ "penki-[oO]sios", "penktosios" },
	{ "�e�i-[oO]sios", "�e�tosios" },
	{ "septyni-[oO]sios", "septintosios" },
	{ "a�tuoni-[oO]sios", "a�tuntosios" },
	{ "devyni-[oO]sios", "devintosios" },
	{ "de�imt-[oO]sios", "de�imtosios" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-[oO]sios", "$1liktosios" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-[oO]sios", "$1de�imtosios" },
	{ "�imta[si]-[oO]sios", "�imtosios" },
	{ "t�kstantis-[oO]sios", "t�kstantosios" },
	{ "t�kstan�iai-[oO]sios", "t�kstantosios" },

	{ "vienas-[oO]jo", "pirmojo" }, //ivardz vyr kilm NAV 15 eil
	{ "du-[oO]jo", "antrojo" },
	{ "trys-[iI]?ojo", "tre�iojo" },
	{ "keturi-[oO]jo", "ketvirtojo" },
	{ "penki-[oO]jo", "penktojo" },
	{ "�e�i-[oO]jo", "�e�tojo" },
	{ "septyni-[oO]jo", "septintojo" },
	{ "a�tuoni-[oO]jo", "a�tuntojo" },
	{ "devyni-[oO]jo", "devintojo" },
	{ "de�imt-[oO]jo", "de�imtojo" },
	{ "(vienuo|dvy|try|keturio|penkio|�e�io|septynio|a�tuonio|devynio)lika-[oO]jo", "$1liktojo" },
	{ "(dvi|tris|keturias|penkias|�e�ias|septynias|a�tuonias|devynias)de�imt-[oO]jo", "$1de�imtojo" },
	{ "�imta[si]-[oO]jo", "�imtojo" },
	{ "t�kstantis-[oO]jo", "t�kstantojo" },
	{ "t�kstan�iai-[oO]jo", "t�kstantojo" },

		//	{ "#DgsK# #Dgs[KGI]#", "#DgsK#" }, // perkelti atgal
//	{ "#VnsV# #Dgs([KGI]#)", "#Vns$1" },
//	{ "#Vns([KGI]#) #VnsV#", "#Vns$1" }, //new
//	{ "#Dgs([KGI]#) #VnsV#", "#Vns$1" }, //new

	{ "#VnsV# m�n\\.", "m�nuo" }, //m�n turi b�ti prie� m., nes kitaip randa �od�io pabaig� m\\b�n
	{ "#VnsK# m�n\\.", "m�nesio" },
	{ "#VnsG# m�n\\.", "m�nes�" },
	{ "#VnsI# m�n\\.", "m�nesiu" },
	{ "#DgsK# m�n\\.", "m�nesi�" },
	{ "#DgsG# m�n\\.", "m�nesius" },
	{ "#DgsI# m�n\\.", "m�nesiais" },
	{ "\\bm�n\\.", "m�nesiai" }, //DgsV

	{ "#VnsV# mlrd\\b", "milijardas" },
	{ "#VnsK# mlrd\\b", "milijardo" },
	{ "#VnsG# mlrd\\b", "milijard�" },
	{ "#VnsI# mlrd\\b", "milijardu" },
	{ "#DgsK# mlrd\\b", "milijard�" },
	{ "#DgsG# mlrd\\b", "milijardus" },
	{ "#DgsI# mlrd\\b", "milijardais" },
	{ "\\bmlrd\\b", "milijardai" }, //DgsV

	{ "#VnsV# mln\\b", "milijonas" },
	{ "#VnsK# mln\\b", "milijono" },
	{ "#VnsG# mln\\b", "milijon�" },
	{ "#VnsI# mln\\b", "milijonu" },
	{ "#DgsK# mln\\b", "milijon�" },
	{ "#DgsG# mln\\b", "milijonus" },
	{ "#DgsI# mln\\b", "milijonais" },
	{ "\\bmln\\b", "milijonai" }, //DgsV

	{ "#VnsV# tukst\\b", "t�kstantis" },
	{ "#VnsK# tukst\\b", "t�kstan�io" },
	{ "#VnsG# tukst\\b", "t�kstant�" },
	{ "#VnsI# tukst\\b", "t�kstan�iu" },
	{ "#DgsK# tukst\\b", "t�kstan�i�" },
	{ "#DgsG# tukst\\b", "t�kstan�ius" },
	{ "#DgsI# tukst\\b", "t�kstan�iais" },
	{ "\\btukst\\b", "t�kstan�iai" }, //DgsV

	{ "#VnsV# Lt\\b", "Litas" },
	{ "#VnsK# Lt\\b", "Lito" },
	{ "#VnsG# Lt\\b", "Lit�" },
	{ "#VnsI# Lt\\b", "Litu" },
	{ "#DgsK# Lt\\b", "Lit�" },
	{ "#DgsG# Lt\\b", "Litus" },
	{ "#DgsI# Lt\\b", "Litais" },
	{ "\\bLt\\b", "Litai" }, //DgsV

	{ "#VnsV# USD\\b", "doleris" },
	{ "#VnsK# USD\\b", "dolerio" },
	{ "#VnsG# USD\\b", "doler�" },
	{ "#VnsI# USD\\b", "doleriu" },
	{ "#DgsK# USD\\b", "doleri�" },
	{ "#DgsG# USD\\b", "dolerius" },
	{ "#DgsI# USD\\b", "doleriais" },
	{ "\\bUSD\\b", "doleriai" }, //DgsV

	{ "#VnsV# km/h\\b", "kilometras per valand�" }, //NAV 8 eil
	{ "#VnsK# km/h\\b", "kilometro per valand�" },
	{ "#VnsG# km/h\\b", "kilometr� per valand�" },
	{ "#VnsI# km/h\\b", "kilometru per valand�" },
	{ "#DgsK# km/h\\b", "kilometr� per valand�" },
	{ "#DgsG# km/h\\b", "kilometrus per valand�" },
	{ "#DgsI# km/h\\b", "kilometrais per valand�" },
	{ "\\bkm/h\\b", "kilometrai per valand�" }, //DgsV

	{ "#VnsV# km\\b", "kilometras" },
	{ "#VnsK# km\\b", "kilometro" },
	{ "#VnsG# km\\b", "kilometr�" },
	{ "#VnsI# km\\b", "kilometru" },
	{ "#DgsK# km\\b", "kilometr�" },
	{ "#DgsG# km\\b", "kilometrus" },
	{ "#DgsI# km\\b", "kilometrais" },
	{ "\\bkm\\b", "kilometrai" }, //DgsV

	{ "#VnsV# m\\b", "metras" },
	{ "#VnsK# m\\b", "metro" },
	{ "#VnsG# m\\b", "metr�" },
	{ "#VnsI# m\\b", "metru" },
	{ "#DgsK# m\\b", "metr�" },
	{ "#DgsG# m\\b", "metrus" },
	{ "#DgsI# m\\b", "metrais" },
	{ "\\bm\\b[^������]", "metrai" }, //DgsV

	{ "#VnsV# d\\.", "diena" },
	{ "#VnsK# d\\.", "dienos" },
	{ "#VnsG# d\\.", "dien�" },
	{ "#VnsI# d\\.", "diena" },
	{ "#DgsK# d\\.", "dien�" },
	{ "#DgsG# d\\.", "dienas" },
	{ "#DgsI# d\\.", "dienomis" },
	{ "\\bd\\.", "dienos" }, //DgsV

	{ "#VnsV# val\\b", "valanda" },
	{ "#VnsK# val\\b", "valandos" },
	{ "#VnsG# val\\b", "valand�" },
	{ "#VnsI# val\\b", "valanda" },
	{ "#DgsK# val\\b", "valand�" },
	{ "#DgsG# val\\b", "valandas" },
	{ "#DgsI# val\\b", "valandomis" },
	{ "\\bval\\b", "valandos" }, //DgsV

	{ "#VnsV# min\\b", "minut�" },
	{ "#VnsK# min\\b", "minut�s" },
	{ "#VnsG# min\\b", "minut�" },
	{ "#VnsI# min\\b", "minute" },
	{ "#DgsK# min\\b", "minu�i�" },
	{ "#DgsG# min\\b", "minutes" },
	{ "#DgsI# min\\b", "minut�mis" },
	{ "\\bmin\\b", "minut�s" }, //DgsV

	{ "#VnsV# sek\\b", "sekund�" },
	{ "#VnsK# sek\\b", "sekund�s" },
	{ "#VnsG# sek\\b", "sekund�" },
	{ "#VnsI# sek\\b", "sekunde" },
	{ "#DgsK# sek\\b", "sekund�i�" },
	{ "#DgsG# sek\\b", "sekundes" },
	{ "#DgsI# sek\\b", "sekund�mis" },
	{ "\\bsek\\b", "sekund�s" }, //DgsV

	{ "#VnsV# ?�C\\b", "laipsnis" }, //NAV 8 eil
	{ "#VnsK# ?�C\\b", "laipsnio" },
	{ "#VnsG# ?�C\\b", "laipsn�" },
	{ "#VnsI# ?�C\\b", "laipsniu" },
	{ "#DgsK# ?�C\\b", "laipsni�" },
	{ "#DgsG# ?�C\\b", "laipsnius" },
	{ "#DgsI# ?�C\\b", "laipsniais" },
	{ "�C\\b", "laipsniai" }, //DgsV
//	{ "�", "laipsniai" }, //DgsV

	{ "#VnsV# ?%", " procentas" }, //NAV 8 eil
	{ "#VnsK# ?%", " procento" },
	{ "#VnsG# ?%", " procent�" },
	{ "#VnsI# ?%", " procentu" },
	{ "#DgsK# ?%", " procent�" },
	{ "#DgsG# ?%", " procentus" },
	{ "#DgsI# ?%", " procentais" },
//	{ "\\b%\\b", "procentai" }, //DgsV
	{ "%", " procentai" }, //DgsV

	{ " #VnsV#", "" }, //ismetam nepanaudotus
	{ " #VnsK#", "" },
	{ " #VnsG#", "" },
	{ " #VnsI#", "" },
	{ " #DgsK#", "" },
	{ " #DgsG#", "" },
	{ " #DgsI#", "" },
	{ " #MotG#", "" },
	{ " #MK#", "" },
	{ " #VK#", "" }, //NAV

	{ "\\b([dD]r)\\.", "$1" },
	{ "\\b([pP])(rof)\\.", "$1ro`f" },
	{ "\\b([dD])(oc)\\.", "$1o`c" },
	{ "\\b([lL]iet)\\.", "$1uvi�kai" },	

	{ "Domus Philologiae", "Do`mus Filolo`gija" },
	{ "\\bARKSI\\b", "ARKSI`" },
	{ "\\bBKKI\\b", "B�-K�-K�-I`" }, //������������������
	{ "\\bUKI\\b", "UKI`" },
	{ "\\bTKI\\b", "T�-K�-I`" },
	{ "\\bLKTI\\b", "eL-K�-T�-I`" },
	{ "\\bBA\\b", "bakalauras" },
	{ "\\bMA\\b", "magistras" },
	{ "�v. Jon�", "�vent� Jon�" },
	{ "�v. ([A-Z])", "�vento $1" }, //NAV
		//	{ "\\b([lL]iet)\\.", "$1uvi�kai" },
//		XVIII a.pr.
//		m. m.
//????????????????????		2019 m.sausis



// Inicialai
	{ "(\\s|\\(|\\{|\\]|^)([A-Z���������])\\.", "$1#SpellK#$2" },  //lietuvisku raidziu turetu neaptikti del \\b

 // Santrumpos be balsiu
//	{ "(^|\\n| )([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+)\\b", "$1#SpellN#$2" },  //lietuvisku raidziu turetu neaptikti del \\b

//	{ "(#SpellN#)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])", "$1$2#SpellN#" },        //lietuvisku raidziu turetu neaptikti del \\b
/*	{ "(^|\\n| |-)([hH])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+)\\b", "$1$2a�-$3" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([jJ])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+)\\b", "$1$2ot-$3" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([kK])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+)\\b", "$1$2a-$3" },        //lietuvisku raidziu turetu neaptikti del \\b

	{ "(^|\\n| |-)([bcdgptvz��BCDGPTVZ��])\\b", "$1$2�~" },  //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([flmnrs�FLMNRS�])\\b", "$1e`$2" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([hH])\\b", "$1$2a~�" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([jJ])\\b", "$1$2o`t" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |-)([kK])\\b", "$1$2a~" },        //lietuvisku raidziu turetu neaptikti del \\b
*/
 // Santrumpos be balsiu
/*	{ "(^|\\n| |#)([bcdgptvz��BCDGPTVZ��])(?=[bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+\\b)", "$1$2�#" },  //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([flmnrs�FLMNRS�])(?=[bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+\\b)", "$1e$2#" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([hH])(?=[bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+\\b)", "$1$2a�#" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([jJ])(?=[bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+\\b)", "$1$2ot#" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([kK])(?=[bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���]+\\b)", "$1$2a#" },        //lietuvisku raidziu turetu neaptikti del \\b

	{ "(^|\\n| |#)([bcdgptvz��BCDGPTVZ��])\\b", "$1$2�~" },  //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([flmnrs�FLMNRS�])\\b", "$1e`$2" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([hH])\\b", "$1$2a~�" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([jJ])\\b", "$1$2o`t" },        //lietuvisku raidziu turetu neaptikti del \\b
	{ "(^|\\n| |#)([kK])\\b", "$1$2a~" },        //lietuvisku raidziu turetu neaptikti del \\b
*/
 // Santrumpos be balsiu
/*	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#Spell#$1#Spell#$2#Spell#$3#Spell#$4#Spell#$5#SpellK#$6" }, 
	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#Spell#$1#Spell#$2#Spell#$3#Spell#$4#SpellK#$5" },
	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#Spell#$1#Spell#$2#Spell#$3#SpellK#$4" },
	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#Spell#$1#Spell#$2#SpellK#$3" },
	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#Spell#$1#SpellK#$2" },
	{ "(?:\\n| |^)?([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])\\b", "#SpellK#$1" },
*/
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#Spell#$2#Spell#$3#Spell#$4#Spell#$5#Spell#$6#SpellK#$7$8" },
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#Spell#$2#Spell#$3#Spell#$4#Spell#$5#SpellK#$6$7" },
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#Spell#$2#Spell#$3#Spell#$4#SpellK#$5$6" },
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#Spell#$2#Spell#$3#SpellK#$4$5" },
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#Spell#$2#SpellK#$3$4" },
	{ "(\\s|\\(|\\{|\\]|^)([bcdfghjklmnprstvz���BCDFGHJKLMNPRSTVZ���])(\\s|\\.|,|:|;|\\)|\\}|\\]|$)", "$1#SpellK#$2$3" }, // vietoje \\b

	{ "(#Spell#)([bcdgptvz��BCDGPTVZ��])", "$2�-" }, 
	{ "(#SpellK#)([bcdgptvz��BCDGPTVZ��])", "$2�~" }, 
	{ "(#Spell#)([flmnrs�FLMNRS�])", "e$2-" },
	{ "(#SpellK#)([flmnrs�FLMNRS�])", "e`$2" },
	{ "(#Spell#)([hH])", "$2a�-" },
	{ "(#SpellK#)([hH])", "$2a~�" },
	{ "(#Spell#)([jJ])", "$2ot-" },
	{ "(#SpellK#)([jJ])", "$2o`t" },
	{ "(#Spell#)([kK])", "$2�-" }, 
	{ "(#SpellK#)([kK])", "$2�~" },

	{ "(#SpellK#)([aeoy������AEOY������])", "$2~" },
	{ "(#SpellK#)([iuIU])", "$2`" },

	{ " ?/ ?", " " }, //NAV / i tarpa
	{ "  ", " " }, //NAV du tarpus i viena


// skaidymas fraz�mis
//	{ "(\\. )(.+?)(\\.) ", "$1$2$3\n" },
//	{ "(^|\\n)(.+?)(\\.) ", "$1$2$3\n" }
//	{ "([^\\.,]{20,})([\\.,] )", "$1$2\n" } //20181106

//	{ "ma", "mo" }

};

int main()
{
//	std::string s = "Ra�au ka�kok� tekst�, kur� bandysiu skaidyti fraz�mis, kuri� ilgis ne ilgesnis u� 20 simboli�. Pirmiausiai skaidysiu pagal ta�kus. Tada pagal kablelis ir visokius skliaustus (t.y. kai �od�iai pateikiami skliaustuose). Kitas b�das skaidyti pagal jungiamuosius jungtukus ar, arba, ir, bei. Prie�paskutinis metodas skaidyi pagal �od�i� ribas, o jeiturimka�kok�labaiilg��od�, skaidom pagal raides.";
//	std::string s = "mamamamamamamamamama.";
//	std::string s = "� � � � � � � � �.";
//	std::string s = "Nuo 1941-12-06 apie 2011 05 21 gyvenu gerai.";
//	std::string s = "Nuo 1941 m. gegu��s 21 d. gyvenu gerai.";
//	std::string s = "I) A� ir dar ilgokas sakinys.\nII) Tu.\nIII. Jis ir ji.\nIV. Mes ir dar ilgokas sakinys;\nV) J�s.\nVI) Jie ir jos.";
//	std::string s = "Vardiju:\nI) A� ir dar ilgokas sakinys.\nII) Tu.\nIII. Jis ir ji.\nIV. Mes ir dar ilgokas sakinys;\nV) J�s.\nVI) Jie ir jos.";
//	std::string s = "ir gegu��s 21 d. nuo gruod�io 6 d. gyvenu gerai.";
//	std::string s = "Nuo 1941 m. gegu��s m�n. ir 2011 m. gruod�io m�n. gyvenu gerai.";
//	std::string s = "Pradedant 1941 m. sausio 21 d. gyvenu gerai.";
//	std::string s = "Nuo 32101001013 val apie 1322 min.";
//	std::string s = "Nuo 3 iki 5 km apie 11 su 22 u� 5 km 300 m u� 3 Lt po 3 val 15 min 11 sek.";

	std::string s;
	char cc[10000];

	FILE *fd, *fr;
	fd = fopen("duom.txt", "r");
	if (fd == 0) return -1;
	fr = fopen("rez.txt", "w");
	while (EOF != fscanf(fd, "%[^\n]\n", cc))
	{
		s.assign(cc);

		for (int i = 0; i < rulescount; i++)
		{
			std::regex long_word_regex(Tais[i].KP);
			/*std::string new_*/ s = std::regex_replace(s, long_word_regex, Tais[i].DP);
//			std::cout << /*new_*/ s << '\n'; //NAV
		}
		fprintf(fr, "%s\t%s\n", cc, s.c_str());
	}

	fclose(fd);
	fclose(fr);

/*	for (int i = 0; i < rulescount; i++)
	{
		std::regex long_word_regex(Tais[i].KP);
		s = std::regex_replace(s, long_word_regex, Tais[i].DP);
		if (i>=449) std::cout << s << '\n';
	}*/

	return 0;
}
/*
int main()
{
	std::string s = "Some people, when confronted with a problem, think "
		"\"I know, I'll use regular expressions.\" "
		"Now they have two problems.";

	std::regex self_regex("REGULAR EXPRESSIONS",
		std::regex_constants::ECMAScript | std::regex_constants::icase);
	if (std::regex_search(s, self_regex)) {
		std::cout << "Text contains the phrase 'regular expressions'\n";
	}

	std::regex word_regex("(\\S+)");
	auto words_begin =
		std::sregex_iterator(s.begin(), s.end(), word_regex);
	auto words_end = std::sregex_iterator();

	std::cout << "Found "
		<< std::distance(words_begin, words_end)
		<< " words\n";

	const int N = 6;
	std::cout << "Words longer than " << N << " characters:\n";
	for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
		std::smatch match = *i;
		std::string match_str = match.str();
		if (match_str.size() > N) {
			std::cout << "  " << match_str << '\n';
		}
	}

	std::regex long_word_regex("(\\w{7,})");
	std::string new_s = std::regex_replace(s, long_word_regex, "[$&]");
	std::cout << new_s << '\n';

	return 0;
}*/

//Tais[24] = {
	//*00*/{ "([2-9][2-9])000", "$1tukstancei$2" },
	//*01*/{ "([2-9][2-9])00(\\d)", "$1tukstancei$2" },
	//*02*/{ "([2-9][2-9])0(\\d\\d)", "$1tukstancei$2" },
	//*03*/{ "([2-9][2-9])(\\d\\d\\d)", "$1tukstancei$2" },
	//*04*/{ "([2-9]1)000", "$1tukstantis$2" },
	//*05*/{ "([2-9]1)00(\\d)", "$1tukstantis$2" },
	//*06*/{ "([2-9]1)0(\\d\\d)", "$1tukstantis$2" },
	//*07*/{ "([2-9]1)(\\d\\d\\d)", "$1tukstantis$2" },
	//*08*/{ "(1[1-9])000", "$1tukstanciu$2" },
	//*09*/{ "(1[1-9])00(\\d)", "$1tukstanciu$2" },
	//*10*/{ "(1[1-9])0(\\d\\d)", "$1tukstanciu$2" },
	//*11*/{ "(1[1-9])(\\d\\d\\d)", "$1tukstanciu$2" },
	//*12*/{ "([1-9]0)000", "$1tukstanciu$2" },
	//*13*/{ "([1-9]0)00(\\d)", "$1tukstanciu$2" },
	///*14*/{ "([1-9]0)0(\\d\\d)", "$1tukstanciu$2" },
	//*15*/{ "([1-9]0)(\\d\\d\\d)", "$1tukstanciu$2" },
	//*16*/{ "(\\d+)?1000", "$1tukstantis$2" },
	//*17*/{ "(\\d+)?100(\\d)", "$1tukstantis$2" },
	//*18*/{ "(\\d+)?10(\\d\\d)", "$1tukstantis$2" },
	//*19*/{ "(\\d+)?1(\\d\\d\\d)", "$1tukstantis$2" },
	//*20*/{ "([2-9])000", "$1tukstancei$2" },
	//*21*/{ "([2-9])00(\\d)", "$1tukstancei$2" },
	//*22*/{ "([2-9])0(\\d\\d)", "$1tukstancei$2" },
	//*23*/{ "([2-9])(\\d\\d\\d)", "$1tukstancei$2" } };

/*{ "vienas-�j�\\b", "pirm�j�" },	//analogiska failui skaitm.txt // galima b�t� "pirm�j� #DgsK#" !!!
{ "du-�j�\\b", "antr�j�" },
{ "trys-i?�j�\\b", "tre�i�j�" },
{ "keturi-�j�\\b", "ketvirt�j�" },
{ "penki-�j�\\b", "penkt�j�" },
{ "�e�i-�j�\\b", "�e�t�j�" },
{ "septyni-�j�\\b", "septint�j�" },
{ "a�tuoni-�j�\\b", "a�tunt�j�" },
{ "devyni-�j�\\b", "devint�j�" },
{ "de�imt-�j�\\b", "de�imt�j�" },
{ "vienuolika-�j�\\b", "vienuolikt�j�" },
{ "dvylika-�j�\\b", "dvylikt�j�" },
{ "trylika-�j�\\b", "trylikt�j�" },
{ "keturiolika-�j�\\b", "keturiolikt�j�" },
{ "penkiolika-�j�\\b", "penkiolikt�j�" },
{ "�e�iolika-�j�\\b", "�e�iolikt�j�" },
{ "septyniolika-�j�\\b", "septyniolikt�j�" },
{ "a�tuoniolika-�j�\\b", "a�tuoniolikt�j�" },
{ "devyniolika-�j�\\b", "devyniolikt�j�" },
{ "dvide�imt-�j�\\b", "dvide�imt�j�" },
{ "dvide�imt-�j�\\b", "dvide�imt�j�" },
{ "trisde�imt-�j�\\b", "trisde�imt�j�" },
{ "keturiasde�imt-�j�\\b", "keturiasde�imt�j�" },
{ "penkiasde�imt-�j�\\b", "penkiasde�imt�j�" },
{ "�e�iasde�imt-�j�\\b", "�e�iasde�imt�j�" },
{ "septyniasde�imt-�j�\\b", "septyniasde�imt�j�" },
{ "a�tuoniasde�imt-�j�\\b", "a�tuoniasde�imt�j�" },
{ "devyniasde�imt-�j�\\b", "devyniasde�imt�j�" },
{ "�imtas-�j�\\b", "�imt�j�" },
{ "�imtai-�j�\\b", "�imt�j�" },
{ "t�kstantis-�j�\\b", "t�kstant�j�" },
{ "t�kstan�iai-�j�\\b", "t�kstant�j�" },*/

