Onà, Švč. Mergelės Maīrijos motina.
Šv. Joakimo žmona.
Šventoji (šventė – liepos dvidešimt šeši,
kartu su šv. Joakimu).
Pagal jas, Δ(Onà) buvo Betliejaus kunigo Matano duktė.
Δ(Onà) su Joakimu ilgai neturėjo vaikų
pagaliau jie susilauks dukters (hebrajiškai hannāh – Dievo malonė).
o Δ(Onà) netrukus mirė
Rytuose Δ(Onà) kultas
kultas išplito šeštame amžiuje
Vakaruose – septintame amžiuje
šventė žinoma nuo dvylikto amžiaus
keturioliktame amžiuje paplito visoje Europoje.
tūkstantis penki šimtai aštuoniasdešimt ketvirtais popiežius
Grigalius tryliktasis ją paskelbė privalomą visoje Bažnyčioje.
šešioliktame amžiuje Δ(Onà) kultas buvo labai stiprus Vokietijoje,
prieš jį kovojo M. Liuteris.
Krikščioniškojoje ikonografijoje dažnai vaizduojama Šventosios Šeimos ir Marijos auklėjimo kompozicijose
Kartais kaip atskiras siužetas vaizduojama Δ(Onà) mirtis – Švč. Mergelė Marija įduoda Onai į ranką žvakę, o Kristus ją laimina.
LIETUVOJE Δ(Onà) šventė buvo laikoma naujos duonos diena, per ją buvo kepama naujo rugių derliaus bandelė.
Populiarūs Šv. Onos atlaidai (Alvite, Griškabūdyje, Skaruliuose, Vėžaičiuose).
Irkutsko sritis
Krasnojarsko kraštas,
<a class=""rod"" href="""">Biriusa</a>.
Onà, Ona Stiuart gimė tūkstantis šeši šimtai šešiasdešimt penktais vasario šešta✢(Londonas)
mirė tūkstantis septyni šimtai keturioliktais rugpjūčio pirma ten pat,
Didžiosios Britanijos
karalienė (tūkstantis septyni šimtai antrais–keturiolika).
Karaliaus Jokūbo antrojo duktė.
Jokūbas antrasis
tūkstantis šeši šimtai septyniasdešimt antrais priėmė katalikybę,
bet Δ(Onà) dėdės karaliaus
Karolio antrojo valia liko protestante.
Δ(Onà) ir jos aplinka
parėmė vadinama
Šlovingąją revoliuciją
tūkstantis šeši šimtai aštuoniasdešimt aštuntais
per kurią Jokūbas antrasis buvo nuverstas nuo sosto,
karaliumi tapo Δ(Onà) vyr. sesers Marijos vyras
karalius Vilhelmas trečiasis).
Didelės politinis reikšmės turėjo
dar vaikystėje užsimezgusi Δ(Onà) draugystė
su S. J. Churchill,
vėliau Marlborough kunigaikščio
J. Churchillio žmona.
Δ(Onà) pradėjus valdyti
J. Churchillis paskirtas vadovauti kariuomenei,
sėkmingai dalyvavo Ispanijos įpėdinystės kare (tūkstantis septyni šimtai pirmais–keturiolika).
To meto Didžioji Britanija faktiškai buvo konstitucinė monarchija,
bet Δ(Onà) aktyviai dalyvavo formuojant
valstybės vidaus ir užsienio politiką.
Ji aštuoniolika kartų buvo nėščia,
Jam mirus Δ(Onà) tūkstantis septyni šimtai pirmais patvirtino
vadinama Sureguliavimo aktą,
įpėdiniu paskelbtas Jokūbo pirmojo palikuonis
gimė tūkstantis šeši šimtai pirmais rugsėjo dvidešimt antra✢(Valladolid)
mirė tūkstantis šeši šimtai šešiasdešimt šeštais sausio dvidešimta✢(Paryžius),
Prancūzijos karalienė (tūkstantis šeši šimtai penkioliktais–tūkstantis šeši šimtai šešiasdešimt šeštais).
Karaliaus Liudviko tryliktojo žmona.
Ispanijos karaliaus Pilypo trečiojo duktė.
Δ(Onà Áustrė) ir Liudviko tryliktojo vedybos buvo nesėkmingos
pora nuo #DgsK# tūkstantis šeši šimtai dvidešimt
iki vyro mirties (tūkstantis šeši šimtai keturiasdešimt trečiais) gyveno atskirai.
Dėl ispaniškos kilmės Liudviko tryliktojo
pirmasis ministras kardinolas de Richelieu
įtarinėjo Δ(Onà Áustrė) nelojalumu Prancūzijai
siekė apsaugoti Liudviką XIII nuo jos įtakos.
Liudviko tryliktojo motina
Marija Mediči su Δ(Onà Áustrė) nesugebėjo įtikinti jo
Mirus vyrui Δ(Onà Áustrė) tapo
mažamečio sūnaus Liudviko keturioliktojo regente,
pirmuoju ministru paskyrė
italų kilmės
kardinolą J. Mazariną.
Δ(Onà Áustrė) ir jos favoritas buvo slapta susituokę.
Per Frondą Δ(Onà Áustrė) buvo priversta
atleisti J. Mazariną (tūkstantis šeši šimtai penkiasdešimt pirmais),
iki J. Mazarino mirties (tūkstantis šeši šimtai šešiasdešimt pirmais;
Liudvikas keturioliktasis
pilnamečiu paskelbtas tūkstantis šeši šimtai penkiasdešimt pirmais).
Viena pagrindinių A. Dumas romano Trys muškietininkai personažų.
(lenkų Ignacy Żegota Onacewicz)
✞(tūkstantis septyni šimtai aštuoniasdešimt✢(Malaja Berestovica
Valkavisko apskritis
mirė tūkstantis aštuoni šimtai keturiasdešimt penktais vasario aštuoniolikta✢(Sankt Peterburgas),
Filos. magistras
(tūkstantis aštuoni šimtai dešimt; laipsnį įgijo vė u).
Mokėsi Valkavisko ir Gardino mokyklose,
Luko (vokiečių Lyck,
lenkų Ełk)
mokytojų seminarijoje,
tūkstantis aštuoni šimtai šeštais baigė
Karaliaučiaus universiteto
Filosofijos fakultetą.
tūkstantis aštuoni šimtai tryliktais–aštuoniolika Balstogės
Balstogės gimnazijos
senovės kalbų
literatūros,
susipažino su vė u visuotinės istorijos
pavaduotoju J. Leleweliu.
tūkstantis aštuoni šimtai aštuonioliktais–dvidešimt aštuoni vė u dėstė visuotinę istoriją,
nuo #DgsK# tūkstantis aštuoni šimtai dvidešimt – atskirą Lietuvos istorijos kursą
profesorius (tūkstantis aštuoni šimtai dvidešimt septintais).
Jo studentai buvo A. Mickevičius,
S. Daukantas.
Δ(Onacẽvičius) įtarus buvus jos idėjiniu įkvėpėju,
tūkstantis aštuoni šimtai dvidešimt aštuntais ištremtas iš Vilniaus,
tūkstantis aštuoni šimtai trisdešimt ketvirtais išteisintas.
tūkstantis aštuoni šimtai trisdešimt ketvirtais apsigyvenęs Sankt Peterburge
dirbo Archeografijos k‑joje,
N. Rumiancevo muziejuje.
Bendravo su istorikais S. Daukantu
T. Narbutu.
Δ(Onacẽvičius) savo rašomai daugiatomei Lietuvos istorijai medžiagą rinko Karaliaučiuje,
Gardino gubernija dvaruose,
(tyrimų du konspektus):
išspausdinta tūkstantis aštuoni šimtai keturiasdešimt aštuntais)
Žvilgsnis į Lietuvos D. Kunigaikštystės istoriją
išspausdinta tūkstantis aštuoni šimtai keturiasdešimt devintais–penkiasdešimt).
tūkstantis aštuoni šimtai dvidešimt trečiais–keturiasdešimt penki išleido
penki tomus
J. Albertrandi
Δ(Onacẽvičius) surinktos medžiagos ir rankraščių dalis yra Sankt Peterburgo
Rusų literatūros 
institute.
Δ(Onacẽvičius) padėjo pagrindus Lietuvos istorijai nagrinėti:
reikalavo kritiškai vertinti istorijos šaltinius,
nagrinėti ne tik politinis įvykius,
Kūno ilgis apie  du metrai,
aukštis ties gogu šimtas aštuoni–šimtas dvidešimt šeši cm,
masė du šimtai–du šimtai šešiasdešimt kg.
Nėštumas vienuolika mėn.,
drėgnuoju sezonu veda vienas jauniklį.
dvidešimt vienas amžiaus pradžioje
buvo apie  dvidešimt žmonių
iki devyniolikto amžiaus vid.
Δ(ònai) buvo apie  du tūkstančiai,
dvidešimtame amžiuje keturi dešimtmetyje
šimtas žmonių
Iki dvidešimto amžiaus pr.
Δ(ònai) buvo susiskirstę
į  keturiasdešimt patrilinijinių egzogaminių grupių
(keturiasdešimt–šimtas dvidešimt žmonių),
vienijančių du arba keturi fratrijas.
gimė tūkstantis devyni šimtai trisdešimt antrais vasario antra✢(Mažaičiai (Radviliškio valsčius))
Radviliškio valsčius
mirė tūkstantis devyni šimtai aštuoniasdešimt aštuntais spalio aštunta✢(Akademija (Kėdainių rajonas; palaidotas Radviliškyje)),
Kėdainių rajonas
Daktaras
biomed. metrai.;
ž.ū. metrai. kand. tūkstantis devyni šimtai septyniasdešimt trečiais).
tūkstantis devyni šimtai penkiasdešimt aštuntais–šešiasdešimt šeši Radviliškio rajonas Černiachovskio kolūkio pirmininkas.
tūkstantis devyni šimtai šešiasdešimt trečiais baigė Lietuvos žemės ūkio akademiją.
tūkstantis devyni šimtai šešiasdešimt šeštais–septyniasdešimt septyni Radviliškio rajonas Žemės ūkio valdybos viršininko pavaduotojas.
tūkstantis devyni šimtai septyniasdešimt septintais–aštuoniasdešimt aštuoni Žemdirbystės instituto direktoriaus pavaduotojas.
Paskelbė apie  penkiasdešimt mokslo
šimtas penkiasdešimt mokslo populiarinimo straipsnių.
Parašė monografiją Tręšimas (tūkstantis devyni šimtai aštuoniasdešimt devintais),
su A. Damanskiu,
tūkstantis devyni šimtai aštuoniasdešimt),
Trąšų panaudojimo vakariniame es es r es regione moksliniai pagrindai
su kitais, tūkstantis devyni šimtai aštuoniasdešimt pirmais, rusų kalba
su K. Dambrausku,
<sup>du</sup>tūkstantis devyni šimtai aštuoniasdešimt ketvirtais)
Lietuvos tė es r
žemdirbystės sistemas (tūkstantis devyni šimtai aštuoniasdešimt septintais).
cheminių medžiagų aerodinaminį skleistuvą RL‑aštuoni.
gimė tūkstantis devyni šimtai trisdešimt penktais balandžio pirma✢(Kaunas),
J. Onaitytės tėvas.
tūkstantis devyni šimtai penkiasdešimt aštuntais baigė Leningrado (dabar Sankt Peterburgas) elektrotechnikos ryšių institutą.
tūkstantis devyni šimtai šešiasdešimt pirmais–šešiasdešimt šeši
el es es r ryšių ministerijos vyriausiasis inžinierius,
tūkstantis devyni šimtai šešiasdešimt šeštais–šešiasdešimt aštuoni Kubos ryšių ministro patarėjas.
tūkstantis devyni šimtai šešiasdešimt aštuntais–aštuoniasdešimt šeši
el es es r ryšių ministras.
tūkstantis devyni šimtai aštuoniasdešimt šeštais–devyniasdešimt dirbo treste Lietuvos ryšių statyba vyriausiu inžinieriumi.
Nuo #DgsK# tūkstantis devyni šimtai devyniasdešimt gyvena Rusijos Federacijoje.
tūkstantis devyni šimtai devyniasdešimt–devyniasdešimt aštuoni Maskvos centrinio ryšių mokslo tyrimo instituto generalinio direktoriaus pavaduotojas.
tūkstantis devyni šimtai septyniasdešimt šeštais–aštuoniasdešimt aštuoni kandidatas į Lietuvos komunistų partijos Centro komiteto narius.
tūkstantis devyni šimtai septyniasdešimt pirmais–aštuoniasdešimt šeši el es es r Aukščiausiosios Tarybos deputatas.
gimė tūkstantis devyni šimtai penkiasdešimt ketvirtais vasario vienuolikta✢(Vilnius),
K. Onaičio duktė.
tūkstantis devyni šimtai septyniasdešimt šeštais baigusi
Lietuvos konservatoriją vaidina Kauno dramos teatre.
Lietuvos stendinio šaudymo čempionė (tūkstantis devyni šimtai septyniasdešimt antrais).
J. Vaitkaus
M. Gorkio
J. Grušo
abu tūkstantis devyni šimtai septyniasdešimt aštuntais
H. Ibseno
pagal V. Krėvę, abu tūkstantis devyni šimtai aštuoniasdešimt)
A. Strindbergo Kreditoriai tūkstantis devyni šimtai aštuoniasdešimt pirmais,
T. Wilderio Mūsų miestelis tūkstantis devyni šimtai aštuoniasdešimt antrais,
J. Priestley Laikas ir Konvėjai tūkstantis devyni šimtai aštuoniasdešimt penktais),
G. Varno
H. Ibseno Heda Gabler tūkstantis devyni šimtai devyniasdešimt aštuntais,
E. O’Neillo Gedulas tinka Elektrai tūkstantis devyni šimtai devyniasdešimt devintais,
Nusikaltimas ir bausmė du tūkstančiai ketvirtais, pagal F. Dostojevskį,
D. Loher Nekalti du tūkstančiai šeštais),
kiti režisierių
S. Šaltenio Duokiškio baladės tūkstantis devyni šimtai septyniasdešimt aštuntais,
A. M. Sluckaitės Smėlio klavyrai tūkstantis devyni šimtai devyniasdešimt, pagal J. Bobrowskį,
Sofoklio Karalius Oidipas du tūkstančiai antrais,
Gyvenimas dar prieš akis du tūkstančiai aštuntais, pagal R. Gary)
Moteris ir keturi jos vyrai tūkstantis devyni šimtai aštuoniasdešimt trečiais, režisierius A. Puipa,
Lenkauskienė – Duburys du tūkstančiai devintais, režisierius G. Lukšas)
tė vė
Kaimynai tūkstantis devyni šimtai septyniasdešimt devintais, režisierius penktasis. Bačiulis,
Raudonmedžio rojus tūkstantis devyni šimtai aštuoniasdešimt, režisierius B. Talačka,
Turtuolis, vargšas tūkstantis devyni šimtai aštuoniasdešimt trečiais, režisierius A. Žebriūnas,
Čia mūsų namai tūkstantis devyni šimtai aštuoniasdešimt ketvirtais, režisierius S. Vosylius)
tė vė spektakliuose
L. Franko Karlas ir Ana tūkstantis devyni šimtai aštuoniasdešimt pirmais
T. Williamso Geismų tramvajus tūkstantis devyni šimtai aštuoniasdešimt septintais).
Δ(Onaitýtė) santūriomis raiškos priemonėmis kuria emocionalius, stipraus charakterio personažus,
Lietuvos nac. premija (tūkstantis devyni šimtai devyniasdešimt devintais)
Ordino Už nuopelnus Lietuvai Karininko kryžius (du tūkstančiai trečiais).
gimė tūkstantis penki šimtai dvidešimt trečiais spalio aštuoniolikta✢(Krokuva)
mirė tūkstantis penki šimtai devyniasdešimt šeštais rugsėjo devinta✢(Varšuva (palaidota Krokuvoje, Vavelio Žygimantų koplyčioje)),
gyveno Vavelyje, Plocke (Mazovija), Vilniuje (tūkstantis penki šimtai penkiasdešimt aštuntais–šešiasdešimt keturi).
pavyzdžiui, dėl vedybų su Barbora Radvilaite
Po brolio mirties tūkstantis penki šimtai septyniasdešimt antrais nutrūkus vyriškajai Jogailaičių linijai
Iš pradžių, tūkstantis penki šimtai septyniasdešimt trečiais, ją vesti buvo įsipareigojęs Henrikas Valua,
Per kitą tarpuvaldį taip pat reikalauta,
(pavyzdžiui, tai buvo pažadėjęs Ernestas Habsburgas).
tūkstantis penki šimtai septyniasdešimt penktais gruodžio trylikta Varšuvoje Ona Jogailaitė buvo paskelbta Lenkijos karaliene
tūkstantis penki šimtai septyniasdešimt šeštais gegužės pirma ištekėjo už Lenkijos karaliumi išrinkto Transilvanijos kunigaikščio
tūkstantis penki šimtai aštuoniasdešimt šeštais tapusi našle,
Varšuvoje įsteigė Šv. Onos broliją,
dedikavo savo veikalą Šventųjų gyvenimai (tūkstantis penki šimtai septyniasdešimt devintais)
gimė tūkstantis penki šimtai penkioliktais rugsėjo dvidešimt antra
mirė tūkstantis penki šimtai penkiasdešimt septintais liepos šešiolikta✢(Londonas),
karaliaus Henriko aštuntojo
ketvirtoji žmona (tūkstantis penki šimtai keturiasdešimt vienas–septyni).
Vokietijos v. dalies protestantų lyderio
Klevės kunigaikščio Vilhelmo sesuo.
Henrikas aštuntasis
vedė Δ(Onà Kleviẽtė)
Šv. Romos imperijos
tikėtinai sąjungai.
Vokalba portretistas
Hansas Holbeinas jaunesnysis
nutapė Δ(Onà Kleviẽtė) portretą,
kuris Henrikui VIII patiko.
tūkstantis penki šimtai trisdešimt devintais devyni sudaryta vedybų sutartis,
Henrikas aštuntasis pirmąkart
pamatė Δ(Onà Kleviẽtė) tūkstantis penki šimtai keturiasdešimt sausio pirma,
Greitai Henriką VIII
pradėjo erzinti Δ(Onà Kleviẽtė) išminties stoka
menkai mokama anglų kalba,
Šv. Romos imperijos
Prancūzijos s‑ga nebuvo sukurta,
politinis naudos vedybos nedavė.
Anglijos bažnyčia susirinkimas santuoką panaikino.
Δ(Onà Kleviẽtė) sutiko priimti karaliaus sesers titulą,
onanzmas
(Pradžios knyga trisdešimt aštuoni),
tai yra ne masturbuodavosi, bet nutraukdavo lytinį aktą.
mirė tūkstantis keturi šimtai aštuonioliktais✢(Trakai (palaidota Vilniaus arkikatedroje)),
Vedybos veikiausiai įvyko apie  tūkstantis trys šimtai septyniasdešimt penktais–septyniasdešimt šeši.
Jų vienintelė duktė Sofija jau tūkstantis trys šimtai devyniasdešimt
būsimo Maskvos didžiojo kunigaikščio Vasilijaus pirmasis.
tūkstantis trys šimtai aštuoniasdešimt antrais padėjo jam pabėgti iš Jogailos nelaisvės Krėvos pilyje.
Ona Vytautienė dviem dokumentais patvirtino tūkstantis trys šimtai devyniasdešimt antrais rugpjūčio penkta Astravos sutartį
Lietuvos didžiajam kunigaikščiui Vytautui tūkstantis trys šimtai devyniasdešimt aštuntais sudarant Salyno sutartį
tūkstantis keturi šimtai Ona Vytautienė su gausia palyda
minimi apie  keturi šimtai arklių)
šv. Dorotėjos iš Montau kapą Marienwerderio pilyje.
su šv. Dorotėjos gyvenimo aprašymu;
tūkstantis keturi šimtai aštuntais dovanojo Onai Vytautienei klavikordą
tūkstantis keturi šimtai pirmais Vilniaus sutarties dokumente minimos Vytauto dovio teise žmonai užrašytos valdos.
tūkstantis keturi šimtai tryliktais Ona Vytautienė liudijo byloje su Vokiečių ordinu dėl Žemaitijos priklausomybės.
graikiškai oghos – posūkis,
vingis plius  kerkos – uodega),
Δ(onchocerkòzė) serga galvijai,
oncdija (Oncidium),
apie  aštuoni šimtai rūšių.
Kiekvienas stiebas su vienas–trys ilgaamžiais, linijiškai lancetiškais lapais.
dažnai du–trys cm skersmens,
dešimt–keturiasdešimt cm ilgio kekes.
gimė tūkstantis aštuoni šimtai šešiasdešimt devintais lapkričio šešiolikta✢(Oldenburg)
mirė tūkstantis devyni šimtai keturiasdešimt penktais gruodžio dvidešimt aštunta✢(Göttingen),
vokiečių istorikas.
Berlyno ir Heidelbergo universitete studijavo naujųjų laikų istoriją.
tūkstantis aštuoni šimtai devyniasdešimt pirmais−devyniasdešimt keturi
Oldenburgo d. kunigaikščio
centr. archyvo moksl. konsultantas.
Nuo #DgsK# tūkstantis aštuoni šimtai devyniasdešimt aštuntais dėstė Berlyno,
tūkstantis devyni šimtai penktais Čikagos,
tūkstantis devyni šimtai šeštais Giesseno,
nuo #DgsK# tūkstantis devyni šimtai septintais Heidelbergo universitete.
tūkstantis devyni šimtai penkioliktais−aštuoniolika Badeno parlamento atstovų rūmų deputatas;
Po pirmojo pasaulinio karo bendradarbiavo su princu Maksu Badeniečiu
M. Weberiu
kuriant vadinama Weimaro respubliką,
padėjo G. Stresemannui
politiką.
formuoti Vokietijos užsienio politiką.
Nuo #DgsK# tūkstantis devyni šimtai dvidešimt trečiais dėstė Miuncheno,
tūkstantis devyni šimtai dvidešimt aštuntais−trisdešimt penki Berlyno universitete
Svarb. veikalai:
Lasalle. Zwischen Marx und Bismarck tūkstantis devyni šimtai ketvirtais),
Rudolf von Bennigsen tūkstantis devyni šimtai dešimt),
Imperatoriaus Napoleono trečiojo Reino politika
Die Rheinpolitik Kaiser Napoleons III tūkstantis devyni šimtai dvidešimt šeštais)
Das Deutsche Reich und die Vorgeschichte des Weltkrieges tūkstantis devyni šimtai dvidešimt septintais).
gimė tūkstantis devyni šimtai keturiasdešimt trečiais rugsėjo dvylikta✢(Kolombas (Šri Lanka)),
Nuo #DgsK# tūkstantis devyni šimtai šešiasdešimt antrais gyvena Kanadoje.
Studijavo Toronto u-te.
Nuo #DgsK# tūkstantis devyni šimtai septyniasdešimt pirmais dėsto
Yorko u-te Toronte.
Išgarsėjo poezijos rinkiniais
Žavieji monstrai (The Dainty Monsters tūkstantis devyni šimtai šešiasdešimt septintais),
Žmogus su septyniais kojų pirštais (The Man with Seven Toes tūkstantis devyni šimtai šešiasdešimt devintais),
Žiurkių drebučiai (Rat Jelly tūkstantis devyni šimtai septyniasdešimt trečiais).
Kn. Rinktiniai Bilio Kido darbai (The Collected Works of Billy the Kid tūkstantis devyni šimtai septyniasdešimt)
poetinis pasakojimas apie žinomą nusikaltėlį.
Knygoje Šeimos ypatybės (Running in the Family tūkstantis devyni šimtai aštuoniasdešimt antrais)
eilėraščiais ir fotografijomis pasakojama Δ(Ondaatje) tėvų ir senelių gyvenimo istorija kolonijiniame Ceilone.
Kiti poezijos rinkiniai:
Yra toks triukas su peiliu, kurį bandau išmokti (There’s a Trick with a Knife I’metrai Learning to Do tūkstantis devyni šimtai septyniasdešimt devintais),
Pasaulietinė meilė (Secular Love tūkstantis devyni šimtai aštuoniasdešimt ketvirtais),
Rankraščiai (Handwriting tūkstantis devyni šimtai devyniasdešimt aštuntais).
Romane Išgyventi skerdynes (Coming Through Slaughter tūkstantis devyni šimtai septyniasdešimt šeštais)
vaizduojamas Naujojo Orleano džiazo muzikanto
Anglas ligonis (The English Patient tūkstantis devyni šimtai devyniasdešimt antrais,
lietuvių tūkstantis devyni šimtai devyniasdešimt šeštais,
ilmas tūkstantis devyni šimtai devyniasdešimt šeštais,
režisierius A. Minghella),
kelių žmonių gyvenimo istorijos II pasaulinio karo fone.
Kt. romanai:
Liūto kailyje (In the Skin of a Lion tūkstantis devyni šimtai aštuoniasdešimt septintais,
apie  dvidešimtame amžiuje keturi dešimtmečio Torontą),
Anilės šmėkla (Anil’s Ghost du tūkstančiai,
veiksmas vyksta pilietuvių karo draskomoje Šri Lankoje),
Divisadero (du tūkstančiai septintais,
lietuvių du tūkstančiai devintais).
Δ(Ondaatje) kūriniuose dažnai susipina arba priešpriešinama faktai ir fantazija,
t.p. draugystės temos,
Parašė literatūros ir meno kritikos veikalų.
Kūno ilgis iki #DgsK# trisdešimt penki cm,
uodegos – iki #DgsK# dvidešimt aštuoni cm,
masė iki #DgsK# vienas kablelis aštuoni kg.
Po vandeniu gali išbūti dvidešimt minučių.
Per metus veda vienas–keturi (šiltesniame klimate iki #DgsK# šeši) vadų
po keturi–aštuoni jauniklius.
Iki dvidešimto amžiaus ondatra buvo paplitusi tik Šiaurės Amerikoje,
tūkstantis devyni šimtai penktais atvežta į Europą,
tūkstantis devyni šimtai penkiasdešimt ketvirtais
tūkstantis devyni šimtai penkiasdešimt šeštais
dvidešimtame amžiuje pabaigoje
dvidešimt pirmame amžiuje pradžioje ondatrų sumažėjo,
į p.r. nuo Ibadano.
keturi šimtai devyniasdešimt aštuoni šimtas
gyventojai
(du tūkstančiai dešimt).
Aukštosios mokyklos.
Δ(Ondo) apylinkėse auginama javai,
O’Neal Ryan (Rajanas O’Nlas)
gimė tūkstantis devyni šimtai keturiasdešimt pirmais balandžio dvidešimta✢(Los Andželas),
Nuo #DgsK# šeši dešimtmečio pab.
vaidino tė vė serialuose.
Meilės istorija (tūkstantis devyni šimtai septyniasdešimt,
režisierius A. Hilleris)
Oliverio istorija (tūkstantis devyni šimtai septyniasdešimt aštuntais,
režisierius J. Korty).
Kas geresnio, daktare? tūkstantis devyni šimtai septyniasdešimt antrais)
Popierinis mėnulis tūkstantis devyni šimtai septyniasdešimt trečiais),
Nikelodeonas
visų režisierius P. Bogdanovichius),
Baris Lindonas tūkstantis devyni šimtai septyniasdešimt penktais,
režisierius S. Kubrickas),
Vairuotojas tūkstantis devyni šimtai septyniasdešimt septintais,
režisierius W. Hillis),
(Svarbiausias įvykis tūkstantis devyni šimtai septyniasdešimt devintais,
režisierius H. Zieffas),
Partneriai tūkstantis devyni šimtai aštuoniasdešimt antrais,
režisierius J. Burrowsas),
Kieti vyrukai nešoka tūkstantis devyni šimtai aštuoniasdešimt septintais,
režisierius N. Maileris),
Ištikimoji tūkstantis devyni šimtai devyniasdešimt šeštais,
režisierius P. Mazursky).
OnegàUpė Rusijos
europinės dalies š. vakaruose,
Ilgis keturi šimtai šešiolika kilometrų,
baseino plotas penkiasdešimt šeši devyni šimtai kvadratinių kilometrų.
Upė Rusijos europinės dalies š. vakaruose,
Išteka iš Lačios ežeras
į r.
nuo Onegos ežeras),
teka į š. per kalvotą moreninę lygumą.
Įteka į Baltosios jūra
Onegos įlanka
dvidešimt kilometrų ilgio žemupio ruožas
teka du vagomis
Didžioji Onega
Mažoji Onega).
Vid. debitas
penki šimtai penki kubiniai metrai/s.
Užšalusi lapkritį, gruodį–balandį (arba gegužę).
Prie Δ(Onegà) ištakos yra Kargopolio miestas,
europinės dalies š. vakaruose,
Plotas du tūkstančiai septyni šimtai dvidešimt kvadratinių kilometrų
ilgis du šimtai keturiasdešimt aštuoni kilometrai,
didžiausias plotis
plotis devyniasdešimt du kilometrai,
vid. gylis
gylis trisdešimt metrų,
gylis šimtas dvidešimt septyni metrai,
tūris du šimtai devyniasdešimt du kubiniai kilometrai.
Š. ir š.v. krantai aukšti,
p. ir p.r. – žemi,
apie  tūkstantis trys šimtai septyniasdešimt salų ir šcherų
bendras plotas apie  du šimtai penkiasdešimt kvadratinių kilometrų).
Didž. salos:
yra medinės archit-ros muziejus
Į Δ(Onègos ẽžeras) įteka Šuja,
Iš Δ(Onègos ẽžeras)
į Ladogos ežeras
svyravimo amplitudė iki #DgsK# vienas kablelis devyni metrai,
vid. metinė
pusė metrai.
Δ(Onègos ẽžeras) – svarbaus vandens kelio dalis:
Onegos kanalas palei Δ(Onègos ẽžeras) p. krantą
Pakrančių didžiausias miestai:
Onègos lanka (Onežskaja guba),
Arkties vandenynas
Baltosios jūra dalis prie Rusijos
Karelijos ir Archangelsko sritis
į v.
nuo Onegos pusiasalio.
Δ(Onègos lanka) ilgis šimtas aštuoniasdešimt penki kilometrai,
plotis penkiasdešimt–šimtas kilometrų,
didžiausias gylis
šimtas metrų.
Onegos pusiasalio – pelkėti.
Į Δ(Onègos lanka) įteka Onega,
Žiemą Δ(Onègos lanka) užšalusi.
Onègos urv piešinia
Atrasti tūkstantis aštuoni šimtai keturiasdešimt aštuntais.
dvidešimt kilometrų ilgio ruože
yra dvidešimt trys grupės,
daugiau neitūkstantis du šimtai piešinių
ir ženklų (du tūkstančiai ketvirtais).
apskritimas arba pusmėnulis su du spinduliais
dvidešimt–trisdešimt cm pločio,
vienas–du milimetrai gylio Onegos urvų piešiniai iškalti akmeniniais instrumentais.
Datuojami keturi–du tūkstantmečiu prieš Kristų.
Onegos urvų piešiniai priklauso Pudožsko A. F. Korabliovo istorijos ir kraštotyros muziejui.