Onà, Švč. Mergelės Maīrijos motina.
Šv. Joakimo žmona.
Šventoji (šventė – liepos 26,
kartu su šv. Joakimu).
Pagal jas, Δ(Onà) buvo Betliejaus kunigo Matano duktė.
Δ(Onà) su Joakimu ilgai neturėjo vaikų
pagaliau jie susilauks dukters (hebr. hannāh – Dievo malonė).
o Δ(Onà) netrukus mirė
Rytuose Δ(Onà) kultas
kultas išplito 6 a.
Vakaruose – 7 a.
šventė žinoma nuo 12 a.
14 a. paplito visoje Europoje.
1584 popiežius
Grigalius XIII ją paskelbė privalomą visoje Bažnyčioje.
16 a. Δ(Onà) kultas buvo labai stiprus Vokietijoje,
prieš jį kovojo M. Liuteris.
Krikščioniškojoje ikonografijoje dažn. vaizduojama Šventosios Šeimos ir Marijos auklėjimo kompozicijose
Kartais kaip atskiras siužetas vaizduojama Δ(Onà) mirtis – Švč. Mergelė Marija įduoda Onai į ranką žvakę, o Kristus ją laimina.
LIETUVOJE Δ(Onà) šventė buvo laikoma naujos duonos diena, per ją buvo kepama naujo rugių derliaus bandelė.
Populiarūs Šv. Onos atlaidai (Alvite, Griškabūdyje, Skaruliuose, Vėžaičiuose).
Irkutsko sr.
Krasnojarsko kr.,
<a class=""rod"" href=""0"">Biriusa</a>.
Onà, Ona Stiuart ✞(1665 02 06✢(Londonas))
✟(1714 08 01✢(ten pat)),
D. Britanijos
karalienė (1702–14).
Karaliaus Jokūbo II duktė.
Jokūbas II
1672 priėmė katalikybę,
bet Δ(Onà) dėdės karaliaus
Karolio II valia liko protestante.
Δ(Onà) ir jos aplinka
parėmė vad.
Šlovingąją rev‑ją
1688
per kurią Jokūbas II buvo nuverstas nuo sosto,
karaliumi tapo Δ(Onà) vyr. sesers Marijos vyras
karalius Vilhelmas III).
Didelės polit. reikšmės turėjo
dar vaikystėje užsimezgusi Δ(Onà) draugystė
su S. J. Churchill,
vėliau Marlborough kun‑čio
J. Churchillio žmona.
Δ(Onà) pradėjus valdyti
J. Churchillis paskirtas vadovauti kariuomenei,
sėkmingai dalyvavo Ispanijos įpėdinystės kare (1701–14).
To meto D. Britanija faktiškai buvo konstitucinė monarchija,
bet Δ(Onà) aktyviai dalyvavo formuojant
valstybės vidaus ir užs. politiką.
Ji 18 kartų buvo nėščia,
Jam mirus Δ(Onà) 1701 patvirtino
vad. Sureguliavimo aktą,
įpėdiniu paskelbtas Jokūbo I palikuonis
✞(1601 09 22✢(Valladolid))
✟(1666 01 20✢(Paryžius)),
Prancūzijos karalienė (1615–1666).
Karaliaus Liudviko XIII žmona.
Ispanijos karaliaus Pilypo III duktė.
Δ(Onà Áustrė) ir Liudviko XIII vedybos buvo nesėkmingos
pora nuo 1620
iki vyro mirties (1643) gyveno atskirai.
Dėl ispaniškos kilmės Liudviko XIII
pirmasis m‑ras kardinolas de Richelieu
įtarinėjo Δ(Onà Áustrė) nelojalumu Prancūzijai
siekė apsaugoti Liudviką XIII nuo jos įtakos.
Liudviko XIII motina
Marija Mediči su Δ(Onà Áustrė) nesugebėjo įtikinti jo
Mirus vyrui Δ(Onà Áustrė) tapo
mažamečio sūnaus Liudviko XIV regente,
pirmuoju m‑ru paskyrė
it. kilmės
kardinolą J. Mazariną.
Δ(Onà Áustrė) ir jos favoritas buvo slapta susituokę.
Per Frondą Δ(Onà Áustrė) buvo priversta
atleisti J. Mazariną (1651),
iki J. Mazarino mirties (1661;
Liudvikas XIV
pilnamečiu paskelbtas 1651).
Viena pagrindinių A. Dumas romano Trys muškietininkai personažų.
(lenk. Ignacy Żegota Onacewicz)
✞(1780✢(Malaja Berestovica
Valkavisko apskr.
✟(1845 02 18✢(Sankt Peterburgas)),
Filos. magistras
(1810; laipsnį įgijo VU).
Mokėsi Valkavisko ir Gardino m‑lose,
Luko (vok. Lyck,
lenk. Ełk)
mkt. seminarijoje,
1806 baigė
Karaliaučiaus u‑to
Filosofijos f‑tą.
1813–18 Balstogės
Balstogės g‑jos
sen. kalbų
lit‑ros,
susipažino su VU visuotinės istorijos
pavaduotoju J. Leleweliu.
1818–28 VU dėstė visuotinę istoriją,
nuo 1820 – atskirą Lietuvos istorijos kursą
prof. (1827).
Jo studentai buvo A. Mickevičius,
S. Daukantas.
Δ(Onacẽvičius) įtarus buvus jos idėjiniu įkvėpėju,
1828 ištremtas iš Vilniaus,
1834 išteisintas.
1834 apsigyvenęs Sankt Peterburge
dirbo Archeografijos k‑joje,
N. Rumiancevo muziejuje.
Bendravo su istorikais S. Daukantu
T. Narbutu.
Δ(Onacẽvičius) savo rašomai daugiatomei Lietuvos istorijai medžiagą rinko Karaliaučiuje,
Gardino gub. dvaruose,
(tyrimų 2 konspektus):
išsp. 1848)
Žvilgsnis į Lietuvos D. Kunigaikštystės istoriją
išsp. 1849–50).
1823–45 išleido
5 tomus
J. Albertrandi
Δ(Onacẽvičius) surinktos medžiagos ir rankraščių dalis yra Sankt Peterburgo
Rusų lit‑ros 
i‑te.
Δ(Onacẽvičius) padėjo pagrindus Lietuvos istorijai nagrinėti:
reikalavo kritiškai vertinti ist. šaltinius,
nagrinėti ne tik polit. įvykius,
Kūno ilgis ~2 m,
aukštis ties gogu 108–126 cm,
masė 200–260 kg.
Nėštumas 11 mėn.,
drėgnuoju sezonu veda 1 jauniklį.
21 a. pr.
buvo ~20 žmonių
iki 19 a. vid.
Δ(ònai) buvo ~4000,
20 a. 4 d‑metyje
100 žm.
Iki 20 a. pr.
Δ(ònai) buvo susiskirstę
į 40 patrilinijinių egzogaminių grupių
(40–120 žm.),
vienijančių 2 arba 4 fratrijas.
✞(1932 02 02✢(Mažaičiai (Radviliškio vlsč.)))
Radviliškio vlsč.
✟(1988 10 08✢(Akademija (Kėdainių rj.; palaidotas Radviliškyje))),
Kėdainių rj.
Dr.
biomed. m.;
ž.ū. m. kand. 1973).
1958–66 Radviliškio rj. Černiachovskio kolūkio pirmininkas.
1963 baigė Lietuvos žemės ūkio akademiją.
1966–77 Radviliškio rj. Žemės ūkio valdybos viršininko pavaduotojas.
1977–88 Žemdirbystės instituto direktoriaus pavaduotojas.
Paskelbė ~50 mokslo
150 mokslo populiarinimo straipsnių.
Parašė monografiją Tręšimas (1989),
su A. Damanskiu,
1980),
Trąšų panaudojimo vakariniame SSRS regione moksliniai pagrindai
su kitais, 1981, rusų kalba
su K. Dambrausku,
<sup>2</sup>1984)
Lietuvos TSR
žemdirbystės sistemas (1987).
cheminių medžiagų aerodinaminį skleistuvą RL‑8.
✞(1935 04 01✢(Kaunas)),
J. Onaitytės tėvas.
1958 baigė Leningrado (dabar Sankt Peterburgas) elektrotechnikos ryšių institutą.
1961–66
LSSR ryšių ministerijos vyriausiasis inžinierius,
1966–68 Kubos ryšių ministro patarėjas.
1968–86
LSSR ryšių ministras.
1986–90 dirbo treste Lietuvos ryšių statyba vyriausiu inžinieriumi.
Nuo 1990 gyvena Rusijos Federacijoje.
1990–98 Maskvos centrinio ryšių mokslo tyrimo instituto generalinio direktoriaus pavaduotojas.
1976–88 kandidatas į Lietuvos komunistų partijos Centro komiteto narius.
1971–86 LSSR Aukščiausiosios Tarybos deputatas.
✞(1954 02 11✢(Vilnius)),
K. Onaičio duktė.
1976 baigusi
Lietuvos kons‑ją vaidina Kauno dramos teatre.
Lietuvos stendinio šaudymo čempionė (1972).
J. Vaitkaus
M. Gorkio
J. Grušo
abu 1978
H. Ibseno
pagal V. Krėvę, abu 1980)
A. Strindbergo Kreditoriai 1981,
T. Wilderio Mūsų miestelis 1982,
J. Priestley Laikas ir Konvėjai 1985),
G. Varno
H. Ibseno Heda Gabler 1998,
E. O’Neillo Gedulas tinka Elektrai 1999,
Nusikaltimas ir bausmė 2004, pagal F. Dostojevskį,
D. Loher Nekalti 2006),
kt. režisierių
S. Šaltenio Duokiškio baladės 1978,
A. M. Sluckaitės Smėlio klavyrai 1990, pagal J. Bobrowskį,
Sofoklio Karalius Oidipas 2002,
Gyvenimas dar prieš akis 2008, pagal R. Gary)
Moteris ir keturi jos vyrai 1983, rež. A. Puipa,
Lenkauskienė – Duburys 2009, rež. G. Lukšas)
TV
Kaimynai 1979, rež. V. Bačiulis,
Raudonmedžio rojus 1980, rež. B. Talačka,
Turtuolis, vargšas 1983, rež. A. Žebriūnas,
Čia mūsų namai 1984, rež. S. Vosylius)
TV spektakliuose
L. Franko Karlas ir Ana 1981
T. Williamso Geismų tramvajus 1987).
Δ(Onaitýtė) santūriomis raiškos priemonėmis kuria emocionalius, stipraus charakterio personažus,
Lietuvos nac. premija (1999)
Ordino Už nuopelnus Lietuvai Karininko kryžius (2003).
✞(1523 10 18✢(Krokuva))
✟(1596 09 09✢(Varšuva (palaidota Krokuvoje, Vavelio Žygimantų koplyčioje))),
gyveno Vavelyje, Plocke (Mazovija), Vilniuje (1558–64).
pvz., dėl vedybų su Barbora Radvilaite
Po brolio mirties 1572 nutrūkus vyriškajai Jogailaičių linijai
Iš pradžių, 1573, ją vesti buvo įsipareigojęs Henrikas Valua,
Per kitą tarpuvaldį t. p. reikalauta,
(pvz., tai buvo pažadėjęs Ernestas Habsburgas).
1575 12 13 Varšuvoje Ona Jogailaitė buvo paskelbta Lenkijos karaliene
1576 05 01 ištekėjo už Lenkijos karaliumi išrinkto Transilvanijos kunigaikščio
1586 tapusi našle,
Varšuvoje įsteigė Šv. Onos broliją,
dedikavo savo veikalą Šventųjų gyvenimai (1579)
✞(1515 09 22)
✟(1557 07 16✢(Londonas)),
karaliaus Henriko VIII
ketvirtoji žmona (1540 01–07).
Vokietijos v. dalies protestantų lyderio
Klevės kun‑čio Vilhelmo sesuo.
Henrikas VIII
vedė Δ(Onà Kleviẽtė)
Šv. Romos imperijos
tikėtinai s‑gai.
Vok. portretistas
Hansas Holbeinas jaun.
nutapė Δ(Onà Kleviẽtė) portretą,
kuris Henrikui VIII patiko.
1539 09 sudaryta vedybų sutartis,
Henrikas VIII pirmąkart
pamatė Δ(Onà Kleviẽtė) 1540 01 01,
Greitai Henriką VIII
pradėjo erzinti Δ(Onà Kleviẽtė) išminties stoka
menkai mokama anglų k.,
Šv. Romos imperijos
Prancūzijos s‑ga nebuvo sukurta,
polit. naudos vedybos nedavė.
Anglijos bažn. susirinkimas santuoką panaikino.
Δ(Onà Kleviẽtė) sutiko priimti karaliaus sesers titulą,
onanzmas
(Pradžios knyga 38),
t. y. ne masturbuodavosi, bet nutraukdavo lytinį aktą.
✟(1418✢(Trakai (palaidota Vilniaus arkikatedroje))),
Vedybos veikiausiai įvyko ~1375–76.
Jų vienintelė duktė Sofija jau 1390
būsimo Maskvos didžiojo kunigaikščio Vasilijaus I.
1382 padėjo jam pabėgti iš Jogailos nelaisvės Krėvos pilyje.
Ona Vytautienė dviem dokumentais patvirtino 1392 08 05 Astravos sutartį
Lietuvos didžiajam kunigaikščiui Vytautui 1398 sudarant Salyno sutartį
1400 Ona Vytautienė su gausia palyda
minimi ~400 arklių)
šv. Dorotėjos iš Montau kapą Marienwerderio pilyje.
su šv. Dorotėjos gyvenimo aprašymu;
1408 dovanojo Onai Vytautienei klavikordą
1401 Vilniaus sutarties dokumente minimos Vytauto dovio teise žmonai užrašytos valdos.
1413 Ona Vytautienė liudijo byloje su Vokiečių ordinu dėl Žemaitijos priklausomybės.
gr. oghos – posūkis,
vingis + kerkos – uodega),
Δ(onchocerkòzė) serga galvijai,
oncdija (Oncidium),
~800 rūšių.
Kiekvienas stiebas su 1–3 ilgaamžiais, linijiškai lancetiškais lapais.
dažn. 2–3 cm skersmens,
10–40 cm ilgio kekes.
✞(1869 11 16✢(Oldenburg))
✟(1945 12 28✢(Göttingen)),
vok. istorikas.
Berlyno ir Heidelbergo u‑te studijavo naujųjų laikų istoriją.
1891−94
Oldenburgo d. kun‑čio
centr. archyvo moksl. konsultantas.
Nuo 1898 dėstė Berlyno,
1905 Čikagos,
1906 Giesseno,
nuo 1907 Heidelbergo u‑te.
1915−18 Badeno parlamento atstovų rūmų deputatas;
Po I pasaul. karo bendradarbiavo su princu Maksu Badeniečiu
M. Weberiu
kuriant vad. Weimaro respubliką,
padėjo G. Stresemannui
politiką.
formuoti Vokietijos užs. politiką.
Nuo 1923 dėstė Miuncheno,
1928−35 Berlyno u‑te
Svarb. veikalai:
Lasalle. Zwischen Marx und Bismarck 1904),
Rudolf von Bennigsen 1910),
Imperatoriaus Napoleono III Reino politika
Die Rheinpolitik Kaiser Napoleons III 1926)
Das Deutsche Reich und die Vorgeschichte des Weltkrieges 1927).
✞(1943 09 12✢(Kolombas (Šri Lanka))),
Nuo 1962 gyvena Kanadoje.
Studijavo Toronto u-te.
Nuo 1971 dėsto
Yorko u-te Toronte.
Išgarsėjo poez. rinkiniais
Žavieji monstrai (The Dainty Monsters 1967),
Žmogus su septyniais kojų pirštais (The Man with Seven Toes 1969),
Žiurkių drebučiai (Rat Jelly 1973).
Kn. Rinktiniai Bilio Kido darbai (The Collected Works of Billy the Kid 1970)
poet. pasakojimas apie žinomą nusikaltėlį.
Knygoje Šeimos ypatybės (Running in the Family 1982)
eilėraščiais ir fotografijomis pasakojama Δ(Ondaatje) tėvų ir senelių gyvenimo istorija kolonijiniame Ceilone.
Kiti poez. rinkiniai:
Yra toks triukas su peiliu, kurį bandau išmokti (There’s a Trick with a Knife I’m Learning to Do 1979),
Pasaulietinė meilė (Secular Love 1984),
Rankraščiai (Handwriting 1998).
Romane Išgyventi skerdynes (Coming Through Slaughter 1976)
vaizduojamas N. Orleano džiazo muzikanto
Anglas ligonis (The English Patient 1992,
liet. 1996,
ilmas 1996,
rež. A. Minghella),
kelių žmonių gyvenimo istorijos II pasaul. karo fone.
Kt. romanai:
Liūto kailyje (In the Skin of a Lion 1987,
apie 20 a. 4 d-mečio Torontą),
Anilės šmėkla (Anil’s Ghost 2000,
veiksmas vyksta piliet. karo draskomoje Šri Lankoje),
Divisadero (2007,
liet. 2009).
Δ(Ondaatje) kūriniuose dažnai susipina arba priešpriešinama faktai ir fantazija,
t.p. draugystės temos,
Parašė lit-ros ir meno kritikos veikalų.
Kūno ilgis iki 35 cm,
uodegos – iki 28 cm,
masė iki 1,8 kg.
Po vandeniu gali išbūti 20 minučių.
Per metus veda 1–4 (šiltesniame klimate iki 6) vadų
po 4–8 jauniklius.
Iki 20 a. ondatra buvo paplitusi tik Šiaurės Amerikoje,
1905 atvežta į Europą,
1954
1956
20 a. pabaigoje
21 a. pradžioje ondatrų sumažėjo,
į p.r. nuo Ibadano.
498 100
gyv.
(2010).
Aukštosios m‑los.
Δ(Ondo) apylinkėse auginama javai,
O’Neal Ryan (Rajanas O’Nlas)
✞(1941 04 20✢(Los Andželas)),
Nuo 6 d‑mečio pab.
vaidino TV serialuose.
Meilės istorija (1970,
rež. A. Hilleris)
Oliverio istorija (1978,
rež. J. Korty).
Kas geresnio, daktare? 1972)
Popierinis mėnulis 1973),
Nikelodeonas
visų rež. P. Bogdanovichius),
Baris Lindonas 1975,
rež. S. Kubrickas),
Vairuotojas 1977,
rež. W. Hillis),
(Svarbiausias įvykis 1979,
rež. H. Zieffas),
Partneriai 1982,
rež. J. Burrowsas),
Kieti vyrukai nešoka 1987,
rež. N. Maileris),
Ištikimoji 1996,
rež. P. Mazursky).
OnegàUpė Rusijos
europinės dalies š. vakaruose,
Ilgis 416 km,
baseino plotas 56 900 km<sup>2</sup>.
Upė Rusijos europinės dalies š. vakaruose,
Išteka iš Lačios ež.
į r.
nuo Onegos ež.),
teka į š. per kalvotą moreninę lygumą.
Įteka į Baltosios j.
Onegos įl.
20 km ilgio žemupio ruožas
teka 2 vagomis
D. Onega
M. Onega).
Vid. debitas
505 m<sup>3</sup>/s.
Užšalusi lapkritį, gruodį–balandį (arba gegužę).
Prie Δ(Onegà) ištakos yra Kargopolio miestas,
europinės dalies š. vakaruose,
Plotas 9720 km<sup>2</sup>
ilgis 248 km,
didž. plotis
plotis 92 km,
vid. gylis
gylis 30 m,
gylis 127 m,
tūris 292 km<sup>3</sup>.
Š. ir š.v. krantai aukšti,
p. ir p.r. – žemi,
~1370 salų ir šcherų
bendras plotas ~250 km<sup>2</sup>).
Didž. salos:
yra medinės archit-ros muziejus
Į Δ(Onègos ẽžeras) įteka Šuja,
Iš Δ(Onègos ẽžeras)
į Ladogos ež.
svyravimo amplitudė iki 1,9 m,
vid. metinė
0,5 m.
Δ(Onègos ẽžeras) – svarbaus vandens kelio dalis:
Onegos kanalas palei Δ(Onègos ẽžeras) p. krantą
Pakrančių didž. miestai:
Onègos lanka (Onežskaja guba),
Arkties vand.
Baltosios j. dalis prie Rusijos
Karelijos ir Archangelsko sr.
į v.
nuo Onegos p‑lio.
Δ(Onègos lanka) ilgis 185 km,
plotis 50–100 km,
didž. gylis
100 m.
Onegos p‑lio – pelkėti.
Į Δ(Onègos lanka) įteka Onega,
Žiemą Δ(Onègos lanka) užšalusi.
Onègos urv piešinia
Atrasti 1848.
20 km ilgio ruože
yra 23 grupės,
&gt;1200 piešinių
ir ženklų (2004).
apskritimas arba pusmėnulis su 2 spinduliais
20–30 cm pločio,
1–2 mm gylio Onegos urvų piešiniai iškalti akmeniniais instrumentais.
Datuojami 4–2 tūkstantmečiu prieš Kristų.
Onegos urvų piešiniai priklauso Pudožsko A. F. Korabliovo istorijos ir kraštotyros muziejui.