#!/usr/bin/python
# coding=UTF-8

##### EXPN
### reikia padaryti jog tikrintu tiek pagal didžiasias ir mažasias.
### Aprasyti apie tai jog surinkti ne visi linksniai, pagalvoti ar imanoma juos kaip nors isskleisti

trumpiniaiTaskas = {
    #Simboliai
    "+"     : "plius ", 
    "~"     : "apie ",

    # Matavimo vienetai

    "0,5"  : "pusė", 

    ### Amziai 

    "[Š|š]vč." : "Švenčiausioji",
    "[Š|š]v."  : "Šventas",

    "dažn."  : "dažnai",
    "sr."    : "sritis", 
    "prof."  : "profesorius",
    "D. (turima omenyje Britanijos)" : "???", #galun
    "vad."  : "vadinama", #galune
    "polit." : "politinis", #galune
    "užs."  : "užsienio",

    ### Gali nurodyti arba kalbą arba kilmę
    "liet." : "lietuvių",
    "it."   : "italų",
    "lenk." : "lenkų", 
    "vok."  : "vokiečių",
    "gr."   : "graikiškai",
    "hebr." : "hebrajiškai",

    ### Geografines kryptis ir nuorodos
 #   "r."    : "rytai", # rytus, rytu
 #   "š."    : "šiaurė", #šiaurę, šiaurės
 #   "p.r."  : "pietryčiai", # galunė
 #   "v." : "???", #vidurio, vakaru ir t.t. 

    "kr."   : "kraštas",
    "apskr." : "apskritis",
    "mkt."   : "mokytojų",
    "gub."   : "gubernija", #galune
    "filos." : "filosofijos",
    "vlsč." : "valsčius",
    "rj."   : "rajonas",
    "dr." : "daktaras", 
    "kt." : "kitų",
    "pvz." : "pavyzdžiui",
    "t. p." : "taip pat", # reikia look ahead
    "k." : "kalba",
    "sen." : "senovės",
    "ist." : "istorijos",
    "išsp." : "išspausdinta",
    "žm." : "žmonių",

    "jaun." : "jaunesnysis", #galune gali buti jaunesnioji
    "t. y." : "tai yra", # reikia look ahead
    "bažn." : "bažnyčia",
    "svarb." : "svarbiausias", #svarbiausi ir t.t.
    "kn."   :  "knyga",
    "poet."   : "poetinis", #peoetine ir t.t.
    "poez."   : "poezijos",
    "rež."    : "režisierius",
    "kt." : "kiti",

    # Miestai/ Salys
    "N. Orleano" : "Naujojo Orleano", #galunes
    "D. Britanijos" : "Didžiosios Britanijos",
    "D. Britanija"  : "Didžioji Britanija",
    "D. Britaniją"  : "Didžiąją Britaniją",

    "gyv."  : "gyventojai",
    "ež."   : "ežeras", #ežerai, ežero ir t.t.
    "j."    : "jūra", #jūros ir t.t.
    "įl."   : "įlanka", #įlanką
    "D. Onega" : "Didžioji Onega", # Ka daryti su tokiais?
    "M. Onega" : "Mažoji Onega", # Ka daryti su tokiais?
    #"vid." : "vidutinis", # vidutinis. vidituniškai, vidutinė, vidurio
    "didž."  : "didžiausias", #didžiausia, didžiausią
    "vand."  : "vandenynas" # vandenyną ir t.t. 

    


    
}

### Reikia sutvarkyti tarpelius pvz : g-jos, nes nerandame gubernijos, gimnazijos

trumpiniaiBruksnys  = {
    "lit‑ros" :"literatūros", 
    "lit-ros" :"literatūros",
    "S‑gos":"Sąjungos",
    "Dr." : "Daktaras",
    "dr." : "daktaras",
    "archit‑ra":"architektūra",
    "archit‑roje":"architektūroje",
    "archit‑ros":"architektūros",
    "a‑joje":"akademijoje",
    "a‑jos":"akademijos",
    "a‑ją":"akademiją",
    "b‑ka":"biblioteka",
    "b‑kos":"bibliotekos",
    "b‑kų":"bibliotekų",
    "b‑vių":"bendrovių",
    "b‑vė":"bendrovė",
    "b‑vėje":"bendrovėje",
    "b‑vės":"bendrovės",
    "ch‑ka":"charakteristika",
    "ch‑kos":"charakteristikos",
    "ch‑kų":"charakteristikų",
    "dep‑tas":"departamentas",
    "dep‑to":"departamento",
    "d‑ja":"draugija",
    "d‑jos":"draugijos",
    "d‑jų":"draugijų",
    "d‑metis":"dešimtmetis",
    "d‑metyje":"dešimtmetyje",
    "d‑mečio":"dešimtmečio",
    "d-mečio":"dešimtmečio",
    "fed‑ja":"federacija",
    "filh‑jos":"filharmonijos",
    "f‑ja":"funkcija",
    "f‑jai":"funkcijai",
    "f‑jos":"funkcijos",
    "f‑ją":"funkciją",
    "f‑jų":"funkcijų",
    "f‑ku":"", # ???
    "f‑lio":"festivalio",
    "f‑lis":"festivalis",
    "f‑lyje":"festivalyje",
    "f‑nu":"fortepijonu",
    "f‑nui":"fortepijonui",
    "f‑tą":"fakultetą",
    "g‑joje":"gimnazijoje",
    "g‑ją":"gimnaziją",
    "g‑jos": "gimnazijos",
    "g‑tes":"gyvenvietes",
    "g‑tė":"gyvenvietė",
    "g‑tės":"gyvenvientės",
    "i‑tai":"institutai",
    "i‑tas":"institutas",
    "i‑te":"institute",
    "i‑to":"instituto",
    "i‑tą":"institutą",
    "konf‑jų":"konferencijų",
    "kons‑joje":"konservatorijoje",
    "kons‑ją":"konservatoriją",
    "kun‑tes":"kunigaikštystes",
    "kun‑tis":"kunigaikštis",
    "kun‑tė":"kunigaikštė",
    "kun‑tės":"kunigaikštės",
    "kun‑čiai":"kunigaikščiai",
    "kun‑čio":"kunigaikščio",
    "kun‑čių":"kunigaikščių",
    "k‑jos":"komisijos",
    "k-joje": "katedroje",
    "lit‑roje":"literatūroje",
    "lit‑ros":"literatūros",
    "lit‑rą":"literatūrą",
    "l‑piai":"laikotarpiai",
    "l‑piams":"laikotarpiams",
    "l‑pio":"laikotarpio",
    "l‑pis":"laikotarpis",
    "l‑piu":"laikotarpiu",
    "l‑piui":"laikotarpiui",
    "l‑pių":"laikotarpių",
    "l‑pį":"laikotarpį",
    "m‑jai":"ministerijai",
    "m‑la":"mokykla",
    "m‑loje":"mokykloje",
    "m‑los":"mokyklos",
    "m‑lose":"mokyklose",
    "m‑lą":"mokyklą",
    "m‑lų":"mokyklų",
    "m‑ras":"ministras",
    "m‑ro":"ministro",
    "m‑ru":"ministru",
    "org‑ja":"organizacija",
    "org‑jos":"organizacijos",
    "org‑jų":"organizacijų",
    "profs‑gos":"profsąjungos",
    "prov‑ja":"provincija",
    "prov‑jai":"provincijai",
    "prov‑jas":"provincijas",
    "prov‑joje":"provincijoje",
    "prov‑jos":"provincijos",
    "prov‑jose":"provincijose",
    "prov‑ją":"provinciją",
    "p‑lio":"pusiasalio",
    "p‑lių":"pusiasalių",
    "p‑lyje":"pusiasalyje",
    "red‑jose":"redakcijose",
    "rev‑ją":"revoliuciją",
    "s‑bių":"savivaldybių",
    "s‑bės":"savivaldybės",
    "s‑gai":"sąjungai",
    "s‑gos":"sąjungos",
    "s‑ja":"seniūnija",
    "s‑jai":"seniūnijai",
    "s‑joje":"seniūnijoje",
    "s‑jos":"seniūnijos",
    "s‑jų":"seniūnijų",
    #"t‑metyje":"", perziureti
    #"t‑mečio":"", perziureti
    "t‑ra":"temperatūra",
    "t‑rai":"temperatūrai",
    "t‑roje":"temperatūroje",
    "t‑ros":"temperatūros",
    "t‑rą":"temperatūrą",
    "u‑tai":"universitetai",
    "u‑tas":"universitetas",
    "u‑te":"universitete",
    "u‑to":"universiteto",
    "u‑tuose":"universitetuose",
    "u‑tus":"universitetus",
    "u‑tą":"universitetą",
    "vyr‑bė":"vyriausybė",
    "vyr‑bės":"vyriausybės",
    "vyr‑bę":"vyriausybę",
    "v‑ba":"valdyba",
    "v‑bos":"valdybos",
    "v‑ja":"valstija",
    "v‑joje":"valstijoje",
    "v‑jos":"valstijos",
    "v‑jose":"valstijose",
    "v‑jų":"valstijų",
}



#### LSEQ

trumpiniaiLSEQ = {
    "VU" : "vė u",
    "SSRS" : "es es r es",
    "TSR" : "tė es r",
    "LSSR" : "el es es r",
    "TV" : "tė vė"
}