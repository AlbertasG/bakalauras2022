#!/usr/bin/python
# coding=UTF-8


import re
#apibr, term, etimo, termp, lzd lzdp, bio-gim-vt, term term-lt, eqt, bio-mir, vard, vard vard-aut, term term-lot, term term-aut, rodp, bio-gim, bio-mir-vt, pavt
#Rastos tokios klases ^^


#### Trumpiniai


### Reikia stuvarkyti bruksnelius - ir ‑ t.y. suvienodinti

# ../articles-data_original.txt
with open("pre.txt", 'r') as file:
    data = file.read()



### VLE Paragraph

vle_paragraph = '<p>(.*)</p>'
vle_class_term_pattern = '(<span class=\"\"term.+?>)(.+?)(</span>)' #term ir term-lt
vle_italic = '<i>(.+?)</i>'
vle_bold = '<b>(.+?)</b>'
vle_rod_href = '(<a class=\"\"rod"".+?>)(.+?)(</a>)'
#vle_bold2 = '<b>(.+?)</b>'
vle_gime_vt = '(<span class=\"\"bio-gim-vt.+?>)(.+?)(</span>)'
vle_gime = '(<span class=\"\"bio-gim.+?>)(.+?)(</span>)'
vle_mire_vt = '(<span class=\"\"bio-mir-vt.+?>)(.+?)(</span>)'
vle_mire = '(<span class=\"\"bio-mir.+?>)(.+?)(</span>)'
vle_vardas = '(<span class=\"\"vard.+?>)(.+?)(</span>)'
vle_apibr = '(<span class=\"\"apibr.+?>)(.+?)(</span>)'
vle_rodp = '(<span class=\"\"rodp\"\">)(.+?)(</span>)'
vle_etimo = '(<span class=\"\"etimo\"\">)(.+?)(</span>)'
vle_literatura = '<p class=\"\"lit\"\">.*'


article = re.findall(vle_paragraph, data)[25]
vle_class_term = re.search(vle_class_term_pattern, article).group(2)
article = re.compile("Δ").sub("Δ("+vle_class_term+")", article)
article = re.sub(vle_class_term_pattern, r'\2', article)
article = re.sub('&nbsp;'," ", article)# nbsp
article = re.sub('<p class=\"\"aut\"\">.*',"", article)
article = re.sub('</p>|<p>', "", article)
article = re.sub('<p class=\"\"pav\"\".*', "", article) #Isimti paveikslelio aprasymus
article = re.sub(vle_italic, r'\1', article) # isimti pabraukimus <i>
article = re.sub(vle_bold, r'\1', article)
article = re.sub(vle_rod_href, r'\2', article)
article = re.sub(vle_vardas, r'\2', article)
article = re.sub(vle_apibr, r'\2', article)
article = re.sub(vle_rodp, r'\2', article)
article = re.sub(vle_etimo, r'\2', article)
article = re.sub(vle_literatura, "", article)

try:
    vle_gime_vt_term = re.search(vle_gime_vt, article).group(2)
except AttributeError:
    vle_gime_vt_term = ""


if vle_gime_vt_term:
    article = re.compile(vle_gime_vt).sub("✢("+vle_gime_vt_term+")", article)

try:
    vle_gime_term = re.search(vle_gime, article).group(2)
except AttributeError:
    vle_gime_term = ""

if vle_gime_term:     
    article = re.compile(vle_gime).sub("✞("+vle_gime_term+")", article)


try:
    vle_mire_vt_term = re.search(vle_mire_vt, article).group(2)
except AttributeError:
    vle_mire_vt_term = ""

if vle_mire_vt_term:
    article = re.compile(vle_mire_vt).sub("✢("+vle_mire_vt_term+")", article)

try:
    vle_mire_term = re.search(vle_mire, article).group(2)
except AttributeError:
    vle_mire_term = ""

if vle_mire_term:
    article = re.compile(vle_mire).sub("✟("+vle_mire_term+")", article)


#article = re.sub('(Š|š)vč. ', "Švenčiausioji ", article) ### Švč.
#article = re.sub('(Š|š)v. ', "Švento ", article)
#article = re.findall('\d+', article)
#article = re.sub('(liepos)\\b', 'test', article)


print(article)
