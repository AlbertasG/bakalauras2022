#!/usr/bin/python3
# coding=UTF-8

import re
from trumpiniai import trumpiniaiBruksnys, trumpiniaiTaskas, trumpiniaiLSEQ
from skaiciai import romeniskiSkaiciai
from taisykles import taisykles
from taisykles import matavimoVienetai

rule_trumpiniai_bruksnys = "[\wąčęėįšųūžĄČĘĖĮŠŲŲŪŽ]+‑[\wąčęėįšųūĄČĘĖĮŠŲŲŪ]+" # Rasti trumpinius atskirtus bruksniais
rule_trumpiniai_taskas   = "[\wąčęėįšųūžĄČĘĖĮŠŲŲŪŽ]{1,6}\.\s" # Rasti trumpinius - žodžius kurie baigiasi tašku, po jų yra tarpas

def find_all_by_rule(rule, text):
    regex = re.findall(rule, text)

    return regex


def multiple_replace(dict, text):
  # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

  # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text) 



def substitute_regex(rules, text):
    for regex in rules:
      text = re.sub(regex, rules[regex], text)

    return text


def test_funkcija(match):
    group = match.group(1)

    print(type(group))
    
    return group

def skaicius_zodziais(skaicius):
    skaicius = int(skaicius.group(1))

    # neskaiciuosim neigiamu ir itin dideliu skaiciu (iki milijardu)
    if skaicius < 0 or skaicius >= 10 ** 9: return

    skaicius = str(skaicius)

    if skaicius == 0: return 'nulis'

    vienetai = ['', 'vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni']

    niolikai = ['', 'vienuolika', 'dvylika', 'trylika', 'keturiolika', 'penkiolika', 'šešiolika', 'septyniolika',
                'aštuoniolika', 'devyniolika']

    desimtys = ['', 'dešimt', 'dvidešimt', 'trisdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiasdešimt',
                'septyniasdešimt', 'aštuoniasdešimt', 'devyniasdešimt']

    pavadinimai = [
        ['milijonas', 'milijonai', 'milijonų'],
        ['tūkstantis', 'tūkstančiai', 'tūkstančių'],
    ]

    skaicius = '%09d' % int(skaicius)  # iki milijardu 10^9 (milijardu neskaiciuosim)
    skaicius = [skaicius[i:i + 3] for i in range(0, len(skaicius), 3)]

    zodziais = []

    for i, tripletas in enumerate(skaicius):

        # resetinam linksni
        linksnis = 0

        simtai = int(tripletas) // 100
        # pridedam simtu pavadinima, jei pirmas tripleto skaitmuo > 0
        if simtai > 0:
            if simtai > 1:
                zodziais += [vienetai[simtai]]
            zodziais += ['šimtai' if (simtai > 1) else 'šimtas']

        # du paskutiniai tripleto skaiciai
        du = int(tripletas) % 100
        des = du // 10
        vie = du % 10

        # pacekinam nioliktus skaicius
        if 10 < du < 20:
            zodziais += [niolikai[vie]]
            linksnis = 2
        else:
            # pacekinam desimtis
            if des > 0:
                zodziais += [desimtys[des]]

            # pridedam vienetus
            if vie > 0:
                zodziais += [vienetai[vie]]
                linksnis = 1 if (vie > 1) else 0
            else:
                linksnis = 2

        # pridedam pavadinima isskyrus paskutiniam ir nuliniams tripletams
        if i < len(pavadinimai) and tripletas != '000':
            pavadinimas_2 = pavadinimai[i][linksnis]
            zodziais += [pavadinimas_2]

    number = " ".join(zodziais).strip()
    number = number.replace("  ", " ")
    

    return number

def pilna_data_zodziais(skaicius):
    diena   = int(skaicius.group(3))
    menesis = int(skaicius.group(2))
    skaicius = int(skaicius.group(1))

    print(menesis)
    

    
    # neskaiciuosim neigiamu ir itin dideliu skaiciu (iki milijardu)
    if skaicius < 0 or skaicius >= 10 ** 9: return


    skaicius = str(skaicius)
    tukstantis = skaicius[:1]
    skaicius = skaicius[1:]

    if skaicius == 0: return 'nulis'

    vienetai = ['', 'vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni']

    niolikai = ['', 'vienuoliktais', 'dvyliktais', 'tryliktais', 'keturioliktais', 'penkioliktais', 'šešioliktais', 'septynioliktais',
                'aštuonioliktais', 'devynioliktais']

    desimtys = ['', 'dešimt', 'dvidešimt', 'trisdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiasdešimt',
                'septyniasdešimt', 'aštuoniasdešimt', 'devyniasdešimt']

    metai = ['', 'pirmais', 'antrais', 'trečiais', 'ketvirtais', 'penktais', 'šeštais', 'septintais', 'aštuntais', 'devintais']

    menesiai = ["","sausio", "vasario", "kovo", "balandžio", "gegužės", "birželio", "liepos", "rugpjūčio", "rugsėjo", "spalio", "lapkričio", "gruodžio"]

    dienos = ["", "pirma", "antra", "trečia", "ketvirta", "penkta", "šešta", "septinta", "aštunta", "devinta", "dešimta", "vienuolikta", "dvylikta", "trylikta", "keturiolikta", "penkiolikta",
              "šešiolikta", "septyniolikta", "aštuoniolikta", "devyniolikta", "dvidešimta", "dvidešimt pirma", "dvidešimt antra", "dvidešimt trečia", "dvidešimt ketvirta", "dvidešimt penkta", "dvidešimt šeta", "dvidešimt septinta", "dvidešimt aštunta", "dvidešimt devinta", "trisdešimta", "trisdešimt pirma"]

    pavadinimai = [
        ['milijonas', 'milijonai', 'milijonų'],
        ['tūkstantis', 'tūkstančiai', 'tūkstančių'],
    ]

    menesis = menesiai[menesis]
    diena = dienos[diena]

    skaicius = '%09d' % int(skaicius)  # iki milijardu 10^9 (milijardu neskaiciuosim)
    skaicius = [skaicius[i:i + 3] for i in range(0, len(skaicius), 3)]

    if  tukstantis == '1':
        zodziais = ['tūkstantis']
    
    else: 
        zodziais = ['du tūkstančiai']
  
  
    for i, tripletas in enumerate(skaicius):

        # resetinam linksni
        linksnis = 0

        simtai = int(tripletas) // 100

        # pridedam simtu pavadinima, jei pirmas tripleto skaitmuo > 0
        if simtai > 0:
            if simtai > 1:
                zodziais += [vienetai[simtai]]
            zodziais += ['šimtai' if (simtai > 1) else 'šimtas']

        # du paskutiniai tripleto skaiciai
        du = int(tripletas) % 100
        des = du // 10
        vie = du % 10

        # pacekinam nioliktus skaicius
        if 10 < du < 20:
            zodziais += [niolikai[vie]]
            linksnis = 2
        else:
            # pacekinam desimtis
            if des > 0:
                zodziais += [desimtys[des]]

            # pridedam vienetus
            if vie > 0:
                zodziais += [metai[vie]]
                linksnis = 1 if (vie > 1) else 0
            else:
                linksnis = 2

        # pridedam pavadinima isskyrus paskutiniam ir nuliniams tripletams
        if i < len(pavadinimai) and tripletas != '000':
            pavadinimas_2 = pavadinimai[i][linksnis]
            zodziais += [pavadinimas_2]
    number = " ".join(zodziais).strip()
    number = number.replace("  ", " ")

    number = number + " " + menesis + " " + diena

    print(menesis)

    return number


def metai_zodziais(skaicius):
    skaicius = int(skaicius.group(1))
    # neskaiciuosim neigiamu ir itin dideliu skaiciu (iki milijardu)
    if skaicius < 0 or skaicius >= 10 ** 9: return


    skaicius = str(skaicius)
    tukstantis = skaicius[:1]
    skaicius = skaicius[1:]

    if skaicius == 0: return 'nulis'

    vienetai = ['', 'vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni']

    niolikai = ['', 'vienuoliktais', 'dvyliktais', 'tryliktais', 'keturioliktais', 'penkioliktais', 'šešioliktais', 'septynioliktais',
                'aštuonioliktais', 'devynioliktais']

    desimtys = ['', 'dešimt', 'dvidešimt', 'trisdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiasdešimt',
                'septyniasdešimt', 'aštuoniasdešimt', 'devyniasdešimt']

    metai = ['', 'pirmais', 'antrais', 'trečiais', 'ketvirtais', 'penktais', 'šeštais', 'septintais', 'aštuntais', 'devintais']

    pavadinimai = [
        ['milijonas', 'milijonai', 'milijonų'],
        ['tūkstantis', 'tūkstančiai', 'tūkstančių'],
    ]

    skaicius = '%09d' % int(skaicius)  # iki milijardu 10^9 (milijardu neskaiciuosim)
    skaicius = [skaicius[i:i + 3] for i in range(0, len(skaicius), 3)]

    if  tukstantis == '1':
        zodziais = ['tūkstantis']
    
    else: 
        zodziais = ['du tūkstančiai']
  
  
    for i, tripletas in enumerate(skaicius):
        # resetinam linksni
        linksnis = 0

        simtai = int(tripletas) // 100
        # pridedam simtu pavadinima, jei pirmas tripleto skaitmuo > 0
        if simtai > 0:
            if simtai > 1:
                zodziais += [vienetai[simtai]]
            zodziais += ['šimtai' if (simtai > 1) else 'šimtas']

        # du paskutiniai tripleto skaiciai
        du = int(tripletas) % 100
        des = du // 10
        vie = du % 10

        # pacekinam nioliktus skaicius
        if 10 < du < 20:
            zodziais += [niolikai[vie]]
            linksnis = 2
        else:
            # pacekinam desimtis
            if des > 0:
                zodziais += [desimtys[des]]

            # pridedam vienetus
            if vie > 0:
                zodziais += [metai[vie]]
                linksnis = 1 if (vie > 1) else 0
            else:
                linksnis = 2

        # pridedam pavadinima isskyrus paskutiniam ir nuliniams tripletams
        if i < len(pavadinimai) and tripletas != '000':
            pavadinimas_2 = pavadinimai[i][linksnis]
            zodziais += [pavadinimas_2]

    number = " ".join(zodziais).strip()
    number = number.replace("  ", " ")

    return number


if __name__ == "__main__": 


    with open("pre_small.txt", 'r') as f:
        lines = f.readlines()

    with open("post.txt", 'w') as f:
        for line in lines:
            normalize = multiple_replace(trumpiniaiTaskas, line)
            normalize = multiple_replace(trumpiniaiBruksnys, normalize)
            normalize = multiple_replace(trumpiniaiLSEQ, normalize)
            #normalize = multiple_replace(romeniskiSkaiciai, normalize)
            normalize = substitute_regex(taisykles, normalize)
            normalize = re.sub(r'(\d\d\d\d) (\d\d) (\d\d)', pilna_data_zodziais, normalize)
            normalize = re.sub(r'(\d\d\d\d)', metai_zodziais, normalize)
            normalize = re.sub(r'(\d+)', skaicius_zodziais, normalize)
            normalize = substitute_regex(matavimoVienetai, normalize)
            f.writelines(normalize)

    #tekstas = "✟(1845 02 18✢(Sankt Peterburgas)),"
    pilna_data = "1665 02 06"
    #print(re.sub(r'(✟)(\(*\))', r'gime', tekstas))
    #print(skaicius_zodziais(1947))


    print(re.sub(r'(\d\d\d\d) (\d\d) (\d\d)', pilna_data_zodziais, pilna_data))