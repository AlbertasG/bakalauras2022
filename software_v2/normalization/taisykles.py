#!/usr/bin/python3
# coding=UTF-8

def skaicius_zodziais(skaicius):
    skaicius = int(skaicius)

    # neskaiciuosim neigiamu ir itin dideliu skaiciu (iki milijardu)
    if skaicius < 0 or skaicius >= 10 ** 9: return

    skaicius = str(skaicius)

    if skaicius == 0: return 'nulis'

    vienetai = ['', 'vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni']

    niolikai = ['', 'vienuolika', 'dvylika', 'trylika', 'keturiolika', 'penkiolika', 'šešiolika', 'septyniolika',
                'aštuoniolika', 'devyniolika']

    desimtys = ['', 'dešimt', 'dvidešimt', 'trisdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiasdešimt',
                'septyniasdešimt', 'aštuoniasdešimt', 'devyniasdešimt']

    pavadinimai = [
        ['milijonas', 'milijonai', 'milijonų'],
        ['tūkstantis', 'tūkstančiai', 'tūkstančių'],
    ]

    skaicius = '%09d' % int(skaicius)  # iki milijardu 10^9 (milijardu neskaiciuosim)
    skaicius = [skaicius[i:i + 3] for i in range(0, len(skaicius), 3)]

    zodziais = []

    for i, tripletas in enumerate(skaicius):

        # resetinam linksni
        linksnis = 0

        simtai = int(tripletas) // 100
        # pridedam simtu pavadinima, jei pirmas tripleto skaitmuo > 0
        if simtai > 0:
            if simtai > 1:
                zodziais += [vienetai[simtai]]
            zodziais += ['šimtai' if (simtai > 1) else 'šimtas']

        # du paskutiniai tripleto skaiciai
        du = int(tripletas) % 100
        des = du // 10
        vie = du % 10

        # pacekinam nioliktus skaicius
        if 10 < du < 20:
            zodziais += [niolikai[vie]]
            linksnis = 2
        else:
            # pacekinam desimtis
            if des > 0:
                zodziais += [desimtys[des]]

            # pridedam vienetus
            if vie > 0:
                zodziais += [vienetai[vie]]
                linksnis = 1 if (vie > 1) else 0
            else:
                linksnis = 2

        # pridedam pavadinima isskyrus paskutiniam ir nuliniams tripletams
        if i < len(pavadinimai) and tripletas != '000':
            pavadinimas_2 = pavadinimai[i][linksnis]
            zodziais += [pavadinimas_2]

    number = " ".join(zodziais).strip()
    number = number.replace("  ", " ")

    return number


matavimoVienetai = {

    #misc

    r'&gt;' : r'daugiau nei',
    r'&lt;' : r'mažiau nei',

    # Matavimo vienetai
    #^3 kubai
    r'(\w+)(<sup>trys</sup>)' : r'kubiniai \1',
    r'(šimt|lika|šimtai)\b (\w+)(<sup>trys</sup>)' : r'\1 kubinių \2',

    #^2 kvadratai
    r'(šimt|lika|šimtai)\b (\w+)(<sup>du</sup>)' : r'\1 kvadratinių \2',
    r'(\w+)(<sup>du</sup>)' : r'kvadratinių \1',

    # kilomterai
    r'(kvadratinių|kubinių) \b(km)\b' : r'\1 kilometrų',
    r'(kvadratiniai|kubiniai) \b(km)\b' : r'\1 kilometrai',
    r'vienas \b(km)\b' : r'kilometras',
    r'(šimt|lika|šimtas)\b \b(km)\b' : r'\1 kilometrų',
    r'\b(km)\b' : r'kilometrai',
    

    # metrai 
    r'(kvadratinių|kubinių) \b(m)\b' : r'\1 metrų',
    r'(kvadratiniai|kubiniai) \b(m)\b' : r'\1 metrai',
    r'vienas \b(m)\b' : r'metras',
    r'(šimt|lika|šimtas)\b \b(m)\b' : r'\1 metrų',
    r'\b(m)\b' : r'metrai',

    # milimetrai 
    r'(kvadratinių|kubinių) \b(mm)\b' : r'\1 milimetrų',
    r'(kvadratiniai|kubiniai) \b(mm)\b' : r'\1 milimetrai',
    r'vienas \b(mm)\b' : r'milimetras',
    r'(šimt|lika|šimtas)\b \b(mm)\b' : r'\1 milimetrų',
    r'\b(mm)\b' : r'milimetrai',

}

taisykles = {

#    "([a-zA-ZąčęėįšųūžAČĘĖĮŠŲŪŽ])(\d)": r'\1 \2', # pridedam tarpa tarp raides ir skaiciaus
#    "(\d)([a-zA-ZąčęėįšųūžAČĘĖĮŠŲŪŽ])": r'\1 \2', # pridedam tarpa tarp skaiciaus ir raides

    r'\b([Aa]not|[Aa]nt|[Aa]rti|[Aa]ukščiau|[Bb]e|[Dd]ėka|[Dd]ėl|[Dd]ėlei|[Gg]reta|[Ii]ki|[Ll]ig|[Ll]igi|[Ii]s|[Nn]etoli|[Nn]uo|[Pp]asak|[Pp]irmiau) (\d)' : r'\1 #DgsK# \2',
    r'\b([Aa]pie|[Aa]plink|[Aa]plinkui|[Įį]|[Pp]agal|[Pp]alei|[Pp]as|[Pp]askui|[Pp]askum|[Pp]er|[Pp]rieš|[Pp]riešais|[Pp]pro) (\d)' : r'\1 #DgsG# \2',
  
  #Datos
  #  r'\b([Aa]rti|[Ii]ki|[Ll]igi|[Nn]uo|[Tt]arp|[Vv]ietoj|[Uu]ž|[Pp]o) (sausio|vasario|kovo|balandžio|gegužės|birželio|liepos|rugpjūčio|rugsėjo|spalio|lapkričio|gruodžio)\b' : r'\1 #DgsK# \2', 
    r'(#Dgs[KGI]#) ([12]\d\d\d) m\. (sausio|vasario|kovo|balandžio|gegužės|birželio|liepos|rugpjūčio|rugsėjo|spalio|lapkričio|gruodžio) (\d?\d) d\.' : r'$2-ų metų \3 \1 \4 #MK# #VnsV# d.',

  # Misc 

      r'(\d+)(,)(\d+)' : r'\1 kablelis \3',
  #Metai
  #  r'(\#DgsK\#) ([1-2])(\d)(\d)(\d)' : r'TEST',
 
  # Skaičiai
#    r'(#DgsK#) (1)\b' : r' pirmo',
#   r'(#DgsK#) (2)\b' : r' antro',
#    r'(#DgsK#) (3)\b' : r' trečio',
#    r'(#DgsK#) (4)\b' : r' ketvirto',
##    r'(#DgsK#) (5)\b' : r' penkto',
#    r'(#DgsK#) (6)\b' : r' šešto',
#    r'(#DgsK#) (7)\b' : r' septinto',
#    r'(#DgsK#) (8)\b' : r' aštunto',
#    r'(#DgsK#) (9)\b' : r' devinto',
#    r'(#DgsK#) (10)\b' : r' dešimto',
#    r'(#DgsK#) (11)\b' : r' vienuolikto',
##    r'(#DgsK#) (12)' : r' dvylikto',
 #   r'(#DgsK#) (13)\b' : r' trylikto',
 ##   r'(#DgsK#) (14)\b' : r' keturiolikto',
 #   r'(#DgsK#) (15)\b' : r' penkiolikto',
    


    

  #Amziai
    r'(\#DgsK\#) (\d+) (a\.)' : r'\1 \2 amžiaus',
    r'\ba\. pr\.' : r'amžiaus pradžioje',
    r'\ba\. vid\.' : r'amžiaus viduryje',
    r'\ba\. pab\.' : r'amžiaus pabaigoje',
    r'\ba\.' : r'amžiuje',

  r'(\#DgsK\#) (1) (amži*)' : r'pirmo \3',
  r'(\#DgsK\#) (2) (amži*)' : r'antro \3',
  r'(\#DgsK\#) (3) (amži*)' : r'trečio \3',
  r'(\#DgsK\#) (4) (amži*)' : r'ketvirto \3',
  r'(\#DgsK\#) (5) (amži*)' : r'penkto \3',
  r'(\#DgsK\#) (6) (amži*)' : r'šešto \3',
  r'(\#DgsK\#) (7) (amži*)' : r'septinto \3',
  r'(\#DgsK\#) (8) (amži*)' : r'aštunto \3',
  r'(\#DgsK\#) (9) (amži*)' : r'devinto \3',
  r'(\#DgsK\#) (10) (amži*)' : r'dešimto \3',
  r'(\#DgsK\#) (11) (amži*)' : r'vienuolikto \3',
  r'(\#DgsK\#) (12) (amži*)' : r'dvylikto \3',
  r'(\#DgsK\#) (13) (amži*)' : r'trylikto \3',
  r'(\#DgsK\#) (14) (amži*)' : r'keturiolikto \3',
  r'(\#DgsK\#) (15) (amži*)' : r'penkiolikto \3',
  r'(\#DgsK\#) (16) (amži*)' : r'šešiolikto \3',
  r'(\#DgsK\#) (17) (amži*)' : r'septyniolikto \3',
  r'(\#DgsK\#) (18) (amži*)' : r'aštuoniolikto \3',
  r'(\#DgsK\#) (19) (amži*)' : r'devyniolikto \3',
  r'(\#DgsK\#) (20) (amži*)' : r'dvidešimto \3',
  r'(\#DgsK\#) (21) (amži*)' : r'dvidešimt pirmo \3',

  r'\b(1)\b (amžiuje)' : r'pirmame \2',
  r'\b(2)\b (amžiuje)' : r'antrame \2',
  r'\b(3)\b (amžiuje)' : r'trečiame \2',
  r'\b(4)\b (amžiuje)' : r'ketvirtame \2',
  r'\b(5)\b (amžiuje)' : r'penktame \2',
  r'\b(6)\b (amžiuje)' : r'šeštame \2',
  r'\b(7)\b (amžiuje)' : r'septintame \2',
  r'\b(8)\b (amžiuje)' : r'aštuntame \2',
  r'\b(9)\b (amžiuje)' : r'devintame \2',
  r'\b(10)\b (amžiuje)' : r'dešimtama \2',
  r'\b(11)\b (amžiuje)' : r'vienuoliktame \2',
  r'\b(12)\b (amžiuje)' : r'dvyliktame \2',
  r'\b(13)\b (amžiuje)' : r'tryliktame \2',
  r'\b(14)\b (amžiuje)' : r'keturioliktame \2',
  r'\b(15)\b (amžiuje)' : r'penkioliktame \2',
  r'\b(16)\b (amžiuje)' : r'šešioliktame \2',
  r'\b(17)\b (amžiuje)' : r'septynioliktame \2',
  r'\b(18)\b (amžiuje)' : r'aštuonioliktame \2',
  r'\b(19)\b (amžiuje)' : r'devyniolikttame \2',
  r'\b(20)\b (amžiuje)' : r'dvidešimtame \2',
  r'\b(21)\b (amžiuje)' : r'dvidešimt pirmame \2',


  
### NORD Vardininkas vyriska gimine
  r'(as|us)( I)\b' : r'\1 pirmasis',
  r'(as|us)( II)\b' : r'\1 antrasis',
  r'(as|us)( III)\b' : r'\1 trečiasis',
  r'(as|us)( IV)\b' : r'\1 ketvirtasis',
  r'(as|us)( V)\b' : r'\1 penktasis',
  r'(as|us)( VI)\b' : r'\1 šeštasis',
  r'(as|us)( VII)\b' : r'\1 septintasis',
  r'(as|us)( VIII)\b' : r'\1 aštuntasis',
  r'(as|us)( IX)\b' : r'\1 devintasis',
  r'(as|us)( X)\b' : r'\1 dešimtasis',
  r'(as|us)( XI)\b' : r'\1 vienuoliktasis',
  r'(as|us)( XII)\b' : r'\1 dvyliktasis',
  r'(as|us)( XIII)\b' : r'\1 tryliktasis',
  r'(as|us)( XIV)\b' : r'\1 keturioliktasis',
  r'(as|us)( XV)\b' : r'\1 penkelioktasis',
  r'(as|us)( XVI)\b' : r'\1 šešioliktasis',
  r'(as|us)( XVII)\b' : r'\1 septynioliktasis',
  r'(as|us)( XVIII)\b' : r'\1 aštuonioliktasis',
  r'(as|us)( XIX)\b' : r'\1 devynioliktasis',
  r'(as|us)( XX)\b' : r'\1 dvidešimtasis',
  r'(as|us)( XXI)\b' : r'\1 dvidešimt pirmasis',

### NORD Kilmininkas vyriska gimine
  r'(o)( I)\b' : r'\1 pirmojo',
  r'(o)( II)\b' : r'\1 antrojo',
  r'(o)( III)\b' : r'\1 trečiojo',
  r'(o)( IV)\b' : r'\1 ketvirtojo',
  r'(o)( V)\b' : r'\1 penktojo',
  r'(o)( VI)\b' : r'\1 šeštojo',
  r'(o)( VII)\b' : r'\1 septintojo',
  r'(o)( VIII)\b' : r'\1 aštuntojo',
  r'(o)( IX)\b' : r'\1 devintojo',
  r'(o)( X)\b' : r'\1 dešimtojo',
  r'(o)( XI)\b' : r'\1 vienuoliktojo',
  r'(o)( XII)\b' : r'\1 dvyliktojo',
  r'(o)( XIII)\b' : r'\1 tryliktojo',
  r'(o)( XIV)\b' : r'\1 keturioliktojo',
  r'(o)( XV)\b' : r'\1 penkelioktojo',
  r'(o)( XVI)\b' : r'\1 šešioliktojo',
  r'(o)( XVII)\b' : r'\1 septynioliktojo',
  r'(o)( XVIII)\b' : r'\1 aštuonioliktojo',
  r'(o)( XIX)\b' : r'\1 devynioliktojo',
  r'(o)( XX)\b' : r'\1 dvidešimtojo',
  r'(o)( XXI)\b' : r'\1 dvidešimt pirmojo',

### NORD Galininkas vyriska gimine
#  r'(o)( I)\b' : r'\1 pirmojo',
#  r'(o)( II)\b' : r'\1 antrojo',
#  r'(o)( III)\b' : r'\1 trečiojo',
#  r'(o)( IV)\b' : r'\1 ketvirtojo',
#  r'(o)( V)\b' : r'\1 penktojo',
##  r'(o)( VI)\b' : r'\1 šeštojo',
 # r'(o)( VII)\b' : r'\1 septintojo',
 # r'(o)( VIII)\b' : r'\1 aštuntojo',
#  r'(o)( IX)\b' : r'\1 devintojo',
#  r'(o)( X)\b' : r'\1 dešimtojo',
#  r'(o)( XI)\b' : r'\1 vienuoliktojo',
#  r'(o)( XII)\b' : r'\1 dvyliktojo',
#  r'(o)( XIII)\b' : r'\1 tryliktojo',
#  r'(o)( XIV)\b' : r'\1 keturioliktojo',
##  r'(o)( XV)\b' : r'\1 penkelioktojo',
 # r'(o)( XVI)\b' : r'\1 šešioliktojo',
 # r'(o)( XVII)\b' : r'\1 septynioliktojo',
 ## r'(o)( XVIII)\b' : r'\1 aštuonioliktojo',
#  r'(o)( XIX)\b' : r'\1 devynioliktojo',
#  r'(o)( XX)\b' : r'\1 dvidešimtojo',
#  r'(o)( XXI)\b' : r'\1 dvidešimt pirmojo',


#### Naikinam nepanaudotus
  r'(\#DgsG\#)' : r'',

### VLE Spec simboliai

 r'(✟)(\()(.*)(\))' : r'mirė \3',
 r'(✞)(\()(.*)(\))' : r'gimė \3',
 r'(✢\(ten pat\))'  : r' ten pat',
 r'(pasaul.) (karo)' : r'pasaulinio karo'

#    r'(#DgsK#) (12)\b \b(a\.)' : r'dvylikto ',
#    r'(1)\b \b(a\.)' : r'pirmame amžiuje',
#    r'(2)\b \b(a\.)' : r'antrame amžiuje',
#    r'(3)\b \b(a\.)' : r'trečiame amžiuje',
#    r'(4)\b \b(a\.)' : r'ketvirtame amžiuje',
#    r'(5)\b \b(a\.)' : r'penktame amžiuje',
#   r'(6)\b \b(a\.)' : r'šeštame amžiuje',
#    r'(7)\b \b(a\.)' : r'septintame amžiuje',
#    r'(8)\b \b(a\.)' : r'aštuntame amžiuje',
#    r'(9)\b \b(a\.)' : r'devintame amžiuje',
#    r'(10)\b \b(a\.)' : r'dešimtame amžiuje',
#    r'(11)\b \b(a\.)' : r'vienuoliktame amžiuje',
#    r'(12)\b \b(a\.)' : r'dvyliktame amžiuje',
#    r'(13)\b \b(a\.)' : r'tryliktame amžiuje',
#   r'(15)\b \b(a\.)' : r'penkkioliktame amžiuje',
#    r'(16)\b \b(a\.)' : r'šešioliktame amžiuje',
#    r'(17)\b \b(a\.)' : r'septynioliktame amžiuje',
#    r'(18)\b \b(a\.)' : r'aštuonioliktame amžiuje',
#    r'(19)\b \b(a\.)' : r'devynioliktame amžiuje',
#    r'(20)\b \b(a\.)' : r'dvidešimtame amžiuje',
#    r'(21)\b \b(a\.)' : r'dvidešimt pirmame amžiuje',


#    r'(\d) \b(a\.)' : r'\1 amžiuje',
    }

#    r\b([Aa]not|[Aa]nt|[Aa]rti|[Aa]ukščiau|[Bb]e|[Dd]ėka|[Dd]ėl|[Dd]ėlei|[Gg]reta|[Ii]ki|[Ll]ig|[Ll]igi|[Ii]s|[Nn]etoli|[Nn]uo|[Pp]asak|[Pp]irmiau|) (\d)" : r'\1 #DgsK# \2',
#    "\b([Aa]pie|[Aa]plink|[Aa]plinkui|[Įį]|[Pp]agal|[Pp]alei|[Pp]as|[Pp]askui|[Pp]askum|[Pp]er|[Pp]rieš|[Pp]riešais|[Pp]pro) (\d)" : r'\1 #DgsG# \2',
#   "(#Dgs[KGI]#) (\d{1,12})" : r'\1 \2 \1',